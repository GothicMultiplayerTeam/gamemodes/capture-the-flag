local function vob_component(instance, x, y, z) {
    local vob = Vob(instance)
    vob.setPosition(x, y, z)
    vob.addToWorld()
    
    return vob
}

local function flag_text_component(tag, x, y, z) {
    local text = Draw3d(x, y, z)
    text.insertText("")
    text.insertText("")
    text.distance = 2000
    text.visible = true

    if (tag == TeamTag.RED) {
        text.setColor(255, 0, 0)
        text.updateText(0, "Flag: Red team")
    } else if (tag == TeamTag.BLUE) {
        text.setColor(0, 150, 255)
        text.updateText(0, "Flag: Blue team")
    }
    
    return text
}

local function bonus_text_component(bonus, x, y, z) {
    local text = Draw3d(x, y, z)
    text.insertText("")
    text.insertText("")
    text.distance = 500
    text.visible = true

    switch (bonus.type) {
    case BonusType.DAMAGE:
        text.updateText(0, format("Bonus: Damage +%d%%", (bonus.factor - 1.0) * 100))
        break

    case BonusType.SPEED:
        text.updateText(0, format("Bonus: Speed +%d%%", (bonus.factor - 1.0) * 100))
        break

    case BonusType.REGENERATION:
        text.updateText(0, format("Bonus: Health regeneration +%d%%", (bonus.factor - 1.0) * 100))
        break
    }
    
    return text
}

factory <- {
    function createFlag(scene, tag, x, y, z) {
        local entity = scene.create()
        entity.addComponent(FlagTag())
        entity.addComponent(Position(x, y, z))
        entity.addComponent(vob_component(FLAG_VISUAL, x, y, z))
        entity.addComponent(flag_text_component(tag, x, y, z))
        entity.addComponent(Behaviours([behaviour.rotate_vob, behaviour.move_up_down_vob]))

        return entity
    }

    function createBonusPoint(scene, bonus, x, y, z) {
        local entity = scene.create()
        entity.addComponent(BonusPointTag())
        entity.addComponent(Position(x, y, z))
        entity.addComponent(vob_component(BONUS_POINT_VISUAL, x, y - 25, z))
        entity.addComponent(bonus_text_component(bonus, x, y, z))
        entity.addComponent(Behaviours([behaviour.rotate_vob]))

        return entity
    }
}
