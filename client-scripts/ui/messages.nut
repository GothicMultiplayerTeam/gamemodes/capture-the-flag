const MESSAGE_TIMEOUT = 15000

class MessagesBoard {
    _limit = 0
    _messages = null
    _position = null

    constructor(x, y, limit = 10) {
        this._limit = limit
        this._messages = []
        this._position = {x = x, y = y}
    }

    function _refresh() {
        foreach (idx, message in this._messages) {
            message.draw.setPosition(this._position.x, this._position.y + idx * 200)
        }
    }

    function push(r, g, b, message) {
        local draw = Draw(0, 0, message)
        draw.visible = true
        draw.setColor(r, g, b)
        draw.setPosition(this._position.x, this._position.y + this._messages.len() * 200)

        this._messages.push({draw = draw, ts = getTickCount() + MESSAGE_TIMEOUT })
        if (this._messages.len() > this._limit) {
            this._refresh()
        }
    }

    function clear() {
        this._messages.clear()
    }

    function update() {
        local ts = getTickCount()
        local update = false, length = this._messages.len()

        while (length > 0 && this._messages[0].ts <= ts) {
            this._messages.remove(0)

            update = true
            length--
        }

        if (update) {
            this._refresh()
        }
    }
}