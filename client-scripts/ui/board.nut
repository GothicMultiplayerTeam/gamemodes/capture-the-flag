class BoardWindow extends Texture {
	_draws = null

	constructor(lines_count) {
		base.constructor(0, 0, anx(300), 200 * (lines_count + 1), "MENU_INGAME.TGA")

		this._draws = array(lines_count)
		for (local i = 0; i < lines_count; ++i) {
			this._draws[i] = Draw(20, 200 * (i + 1), "")
		}
	}

    function setPosition(x, y) {
        base.setPosition(x, y)

        foreach (i, draw in this._draws) {
            draw.setPosition(x + 100, y + 200 * i + 100)
        }
    }

    function setPositionPx(x, y) {
        base.setPositionPx(x, y)

        foreach (i, draw in this._draws) {
            draw.setPositionPx(x + anx(100), y + any(200) * i + any(100))
        }
    }

    function setVisible(toggle) {
        this.visible = toggle

        foreach (i, draw in this._draws) {
            draw.visible = toggle
        }
    }

	function updateLine(nr, text) {
		this._draws[nr].text = text
	}

    function updateColor(nr, r, g, b) {
        this._draws[nr].setColor(r, g, b)
    }

	function clear() {
		foreach (draw in this._draws) {
			draw.text = ""
		}
	}
}

class BoardLobby extends BoardWindow {
    constructor() {
        base.constructor(12)
    }

    function init() {
        this.clear()

        this.updateLine(0, "Game starts in:")
        this.updateColor(0, 255, 150, 0)

        this.updateLine(3, "Next map:")
        this.updateColor(3, 255, 220, 0)

        this.updateLine(6, "Selected team:")
        this.updateColor(6, 230, 230, 230)

        this.updateLine(9, "Players:")
        this.updateColor(9, 230, 230, 230)
        
        this.updateTeam(TeamTag.NONE)
        this.updatePlayers(TeamTag.RED, 0)
        this.updatePlayers(TeamTag.BLUE, 0)
    }

    function updateTimer(seconds) {
        if (seconds > 0) {
            this.updateLine(0, "Game starts in:")
            this.updateColor(0, 255, 150, 0)
            this.updateColor(1, 255, 255, 255)
            this.updateLine(1, format("%02d:%02d", seconds / 60, seconds % 60))
        } else {
            this.updateColor(0, 0, 255, 0)
            this.updateLine(0, "Game is starting...")
            this.updateLine(1, "")
        }
    }

    function updateMap(name) {
        this.updateColor(4, 200, 200, 200)
        this.updateLine(4, name)
    }

    function updateTeam(team) {
        if (team == TeamTag.RED) {
            this.updateColor(7, 255, 0, 0)
            this.updateLine(7, "Red team")
        } else if (team == TeamTag.BLUE) {
            this.updateColor(7, 0, 150, 255)
            this.updateLine(7, "Blue team")
        } else {
            this.updateColor(7, 200, 200, 200)
            this.updateLine(7, "No team")
        }
    }

    function updatePlayers(team, count) {
        if (team == TeamTag.RED) {
            this.updateLine(10, format("Red team: %d", count))
            this.updateColor(10, 255, 0, 0)
        } else if (team == TeamTag.BLUE) {
            this.updateLine(11, format("Blue team: %d", count))
            this.updateColor(11, 0, 150, 255)
        }
    }
}
