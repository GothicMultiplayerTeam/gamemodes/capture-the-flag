class WinnerLabel {
    _label = null
    _team = null

    constructor(team_tag) {
        this._label = Draw(0, 0, "Winner:")
        this._label.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._label.setPosition(4096 - this._label.width / 2, 4096 - this._label.height)

        this._team = Draw(0, 0, "")
        this._team.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._team.setScale(1.5, 1.5)

        if (team_tag == TeamTag.RED) {
            this._team.text = "Red Team"
            this._team.setColor(255, 0, 0)
            this._label.setColor(255, 0, 0)
        } else if (team_tag == TeamTag.BLUE) {
            this._team.text = "Blue Team"
            this._team.setColor(0, 150, 255)
            this._label.setColor(0, 150, 255)
        }

        this._team.setPosition(4096 - this._team.width / 2, 4096)
    }

    function setVisible(toggle) {
        this._label.visible = toggle
        this._team.visible = toggle
    }
}
