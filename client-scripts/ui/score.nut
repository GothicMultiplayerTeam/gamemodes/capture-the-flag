class ScoreBoard {
    red_score = null
    blue_score = null
    separator = null

    constructor() {
        this.red_score = Draw(0, 0, "")
        this.red_score.font = "FONT_OLD_20_WHITE_HI.TGA"
        this.red_score.setScale(1.5, 1.5)
        this.red_score.setColor(255, 0, 0)

        this.blue_score = Draw(0, 0, "")
        this.blue_score.font = "FONT_OLD_20_WHITE_HI.TGA"
        this.blue_score.setScale(1.5, 1.5)
        this.blue_score.setColor(0, 100, 255)

        this.separator = Draw(0, 0, ":")
        this.separator.font = "FONT_OLD_20_WHITE_HI.TGA"
        this.separator.setScale(1.5, 1.5)
        this.separator.setPosition(4096 - this.separator.width / 2, 8192 - this.separator.height)

        this.reset()
    }

    function setVisible(toggle) {
        this.red_score.visible = toggle
        this.blue_score.visible = toggle
        this.separator.visible = toggle
    }

    function reset() {
        this.updateRedScore(0)
        this.updateBlueScore(0)
    }

    function updateRedScore(value) {
        this.red_score.text = value.tostring()
        this.red_score.setPosition(4096 + this.separator.width, 8192 - this.red_score.height)
    }

    function updateBlueScore(value) {
        this.blue_score.text = value.tostring()
        this.blue_score.setPosition(4096 - this.separator.width - this.blue_score.width, 8192 - this.blue_score.height)
    }
}
