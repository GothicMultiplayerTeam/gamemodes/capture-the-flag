class StatisticsBoard {
    kills = null
    deaths = null
    assists = null

    constructor() {
        this.kills = Draw(50, 7000, "")
        this.deaths = Draw(50, 7200, "")
        this.assists = Draw(50, 7400, "")

        this.reset()
    }

    function setVisible(toggle) {
        this.kills.visible = toggle
        this.deaths.visible = toggle
        this.assists.visible = toggle
    }

    function reset() {
        this.updateKills(0)
        this.updateDeaths(0)
        this.updateAssists(0)
    }

    function updateKills(value) {
        this.kills.text = format("Kills: %d", value)
    }

    function updateDeaths(value) {
        this.deaths.text = format("Deaths: %d", value)
    }

    function updateAssists(value) {
        this.assists.text = format("Assists: %d", value)
    }
}
