class DeadLabel {
    _label = null
    _timer = null

    constructor() {
        this._label = Draw(0, 0, "You died.")
        this._label.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._label.setColor(255, 0, 0)
        this._label.setAlpha(128)
        this._label.setPosition(4096 - this._label.width / 2, 4096 - this._label.height)

        this._timer = Draw(0, 0, "")
        this._timer.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._timer.setColor(255, 0, 0)
        this._timer.setAlpha(128)
        this._timer.setPosition(4096, 4196)
    }

    function setVisible(toggle) {
        this._label.visible = toggle
        this._timer.visible = toggle
    }

    function setRespawnTimeLeft(seconds) {
        this._timer.text = format("Respawn in %d", seconds)
        this._timer.setPosition(4096 - this._timer.width / 2, 4196)
    }
}
