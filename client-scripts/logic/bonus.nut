local counter = 0

class BonusPointLogic {
    _entity = null

    constructor(entity) {
        this._entity = entity
    }

    function startCapture(ms) {
        local text = this._entity.getComponent(Draw3d)
        if (text) {
            this._entity.addComponent(Timer("Capture", ms))
        }
    }

    function stopCapture() {
        local text = this._entity.getComponent(Draw3d)
        // Can be deleted before stop packet arrives, by system :)
        if (text && this._entity.hasComponent(Timer)) {
            this._entity.removeComponent(Timer)
            text.updateText(1, "")
        }
    }

    function finishCapture(pid, team_tag) {
        local text = this._entity.getComponent(Draw3d)
        local vob = this._entity.getComponent(Vob)
        local position = this._entity.getComponent(Position)

        // Clear timer
        text.updateText(1, "")

        if (team_tag == TeamTag.RED) {
            text.setColor(255, 0, 0)
            vob.visual = "ORC_FIREPLACE_V2.3DS"
            vob.setPosition(position.x, position.y - 120, position.z)
        } else if (team_tag == TeamTag.BLUE) {
            text.setColor(0, 150, 255)
            vob.visual = "ORC_FIREPLACE_V1.3DS"
            vob.setPosition(position.x, position.y - 120, position.z)
        } else {
            text.setColor(255, 255, 255)
            vob.visual = BONUS_POINT_VISUAL
            vob.setPosition(position.x, position.y - 25, position.z)
        }

        if (isPlayerStreamed(pid)) {
            addEffect(pid, "spellFX_Teleport_RING")
        }
    }
}
