class FlagLogic {
    _entity = null

    constructor(entity) {
        this._entity = entity
    }

    function startCapture(ms) {
        local text = this._entity.getComponent(Draw3d)
        if (text) {
            this._entity.addComponent(Timer("Capture", ms))
        }
    }

    function stopCapture() {
        local text = this._entity.getComponent(Draw3d)
        if (text) text.updateText(1, "")

        // Can be deleted before stop packet arrives, by system :)
        if (this._entity.hasComponent(Timer)) {
            this._entity.removeComponent(Timer)
        }
    }

    function finishCapture(pid) {
        local text = this._entity.getComponent(Draw3d)
        if (text) text.updateText(1, "")

        // If timer is still going, then remove it.
        // In case when we don't and next message arrives with start,
        // then we've got duplicate component.
        if (this._entity.hasComponent(Timer)) {
            this._entity.removeComponent(Timer)
        }

        if (isPlayerStreamed(pid)) {
            addEffect(pid, "spellFX_Charm")
        }
    }

    function destinationReached(pid, team_tag) {
        if (isPlayerStreamed(pid)) {
            if (team_tag == TeamTag.RED) {
                addEffect(pid, "spellFX_INCOVATION_RED")
            } else if (team_tag == TeamTag.BLUE) {
                addEffect(pid, "spellFX_INCOVATION_BLUE")
            }

            sounds.point.play()
        }
    }

    function attach(pid) {
        this._entity.addComponent(Follow(pid, FLAG_HEIGHT))

        local text = this._entity.getComponent(Draw3d)
        if (text) text.visible = false
    }

    function detach(position) {
        if (this._entity.hasComponent(Follow)) {
            this._entity.removeComponent(Follow)
        }
        
        local current = this._entity.getComponent(Position)
        current.update(position)

        local text = this._entity.getComponent(Draw3d)
        if (text) {
            local vob = this._entity.getComponent(Vob)
            vob.setPosition(position.x, position.y, position.z)

            text.setWorldPosition(position.x, position.y, position.z)
            text.visible = true
        }
    }
}
