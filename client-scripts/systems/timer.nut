function timer_system(entities, timestep) {
    foreach (entity in entities) {
        local text = entity.getComponent(Draw3d)
        local timer = entity.getComponent(Timer)

        if (text == null || timer == null) {
            continue
        }

        timer._last_time_ms = timer.time_left_ms
        timer.time_left_ms -= timestep.delta_ms

        // Should we update text on timer?
        if (timer._last_time_ms / 1000 != timer.time_left_ms / 1000) {
            local minutes = (timer.time_left_ms / 1000) / 60
            local seconds = (timer.time_left_ms / 1000) % 60

            text.updateText(1, format("%s: %02d:%02d", timer.text, minutes, seconds))
        }

        if (timer.time_left_ms <= 0) {
            entity.removeComponent(Timer)
            text.updateText(1, "")
        }
    }
}