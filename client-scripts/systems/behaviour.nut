function behaviour_system(entities, timer) {
    foreach (entity in entities) {
        local behaviours = entity.getComponent(Behaviours)
        if (behaviours) {
            foreach (behaviour in behaviours.entries) {
                behaviour(entity, timer)
            }
        }
    }
}