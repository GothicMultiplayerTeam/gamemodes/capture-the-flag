function follow_system(entities) {
    foreach (entity in entities) {
        local follow = entity.getComponent(Follow)
        local position = entity.getComponent(Position)

        if (follow == null || position == null) {
            continue
        }

        local player_position = getPlayerPosition(follow.npc_id)
        position.x = player_position.x
        position.y = player_position.y + follow.offset
        position.z = player_position.z

        local vob = entity.getComponent(Vob)
        if (vob) {
            vob.setPosition(position.x, position.y, position.z)
        }
    }
}