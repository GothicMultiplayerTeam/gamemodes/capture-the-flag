class GameplayState extends State {
    events = [
        {name = "onRender", handler = "onRender"},
        {name = "onKey", handler = "onKey"},
        {name = "onRespawn", handler = "onRespawn"},
        {name = "player:statistics", handler = "onStatisticsUpdate"},
        {name = "player:dead", handler = "onDead"},
        {name = "gameplay:score", handler = "onScoreUpdate"},
        {name = "gameplay:message", handler = "onMessageUpdate"},
        {name = "entity:capture", handler = "onCaptureUpdate"},
        {name = "entity:follow", handler = "onFollowUpdate"},
        {name = "entity:destination", handler = "onDestinationReached"},
        {name = "entity:team", handler = "onTeamUpdate"},
        {name = "state:lobby", handler = "onLobbyState"},
        {name = "state:finished", handler = "onFinishedState"},
    ]

    _scene = null
    _entitites = null
    _timestep = null
    _map = null
    _map_controller = null
    _score_board = null
    _stats_board = null
    _messages_board = null
    _winner_label = null
    _dead_timer = null
    _stats = null

    constructor(context, data) {
        base.constructor(context)
        this._scene = Scene()
        this._entitites = {}
        this._timestep = Timestep()
        this._map = Map.create(data.map)
        this._map_controller = MapController.create(data.map)

        // TODO: Wyb�r dru�yny po joinie :/
        this._score_board = ScoreBoard()
        this._stats_board = StatisticsBoard()
        this._messages_board = MessagesBoard(50, 4800)
        this._dead_timer = DeadTimer()

        if (data.red_score != null) {
            this._score_board.updateRedScore(data.red_score)
        }

        if (data.blue_score != null) {
            this._score_board.updateBlueScore(data.blue_score)
        }

        foreach (entity in data.entities) {
            if (entity.tag == EntityTag.FLAG) {
                if (this._map_controller) {
                    this._map_controller.spawnFlag(entity.idx, entity.team_tag, entity.position.x, entity.position.z)
                }
                
                this._entitites[entity.idx] <- factory.createFlag(
                    this._scene, entity.team_tag, entity.position.x, entity.position.y, entity.position.z
                )
            } else if (entity.tag == EntityTag.BONUS_POINT) {
                if (this._map_controller) {
                    this._map_controller.spawnBonus(entity.idx, entity.bonus, entity.position.x, entity.position.z)
                }
                
                this._entitites[entity.idx] <- factory.createBonusPoint(
                    this._scene, entity.bonus, entity.position.x, entity.position.y, entity.position.z
                )
            }
        }
    }
    
    /////////////////////////////////////////
    ///	Events
    /////////////////////////////////////////

    function onEnter() {
        this._score_board.setVisible(true)
        this._stats_board.setVisible(true)

        sounds.start_game.play()

        // Clear chat lines
        while (!Chat._lines.empty()) {
            Chat._lines.pop()
        }

        Chat.print(255, 210, 0, "To write message to all players use !message")
        Chat.print(50, 255, 0, "To open map use M key!")
        Chat.print(255, 150, 0, "Capture " + MATCH_POINT + " enemy flags to win round!")

        Discord.activity.state = format("In game: %s - %s", getPlayerName(heroId), this._map.name)
        Discord.activity.timestamps.start = time()
        Discord.activity.timestamps.end = 0
        Discord.activity.update()
    }

    function onExit() {
        this._score_board.setVisible(false)
        this._stats_board.setVisible(false)

        this._dead_timer.label.setVisible(false)
        this._dead_timer.stop()

        if (this._winner_label) {
            this._winner_label.setVisible(false)
        }
    }

    function onRender() {
        this._timestep.update()
        this._messages_board.update()

        if (this._map_controller) {
            this._map_controller.update(this._scene.entities)
        }
        
        follow_system(this._scene.entities)
        behaviour_system(this._scene.entities, this._timestep)
        timer_system(this._scene.entities, this._timestep)
    }

    function onKey(key) {
        if (key == KEY_M && this._map_controller && !chatInputIsOpen()) {
            this._map_controller.toggle()
        }
    }

    function onRespawn() {
        this._dead_timer.label.setVisible(false)

        if (FREECAM_ENABLED) {
            enableFreeCam(false)
        }
    }

    function onDead(data) {
        this._dead_timer.label.setVisible(true)
        this._dead_timer.start(data.seconds)

        sounds.dead[random_int(0, sounds.dead.len() - 1)].play()
        if (FREECAM_ENABLED) {
            enableFreeCam(true)
        }
    }

    function onStatisticsUpdate(data) {
        if (data.kills != null) {
            this._stats_board.updateKills(data.kills)
        }
        
        if (data.deaths != null) {
            this._stats_board.updateDeaths(data.deaths)
        }

        if (data.assists != null) {
            this._stats_board.updateAssists(data.assists)
        }

        if (data.level != null) {
            setLevel(data.level)
        }

        if (data.experience != null) {
            setExp(data.experience)
        }

        if (data.next_experience != null) {
            setNextLevelExp(data.next_experience)
        }

        Discord.activity.state = format("In game: %s - %s", getPlayerName(heroId), this._map.name)
        Discord.activity.update()
    }

    function onScoreUpdate(data) {
        if (data.red_score != null) {
            this._score_board.updateRedScore(data.red_score)
        }

        if (data.blue_score != null) {
            this._score_board.updateBlueScore(data.blue_score)
        }
    }

    function onMessageUpdate(data) {
        this._messages_board.push(data.r, data.g, data.b, data.message)
    }

    function onCaptureUpdate(data) {
        local entity = this._entitites[data.idx]

        if (entity.hasComponent(FlagTag)) {
            local logic = FlagLogic(entity)
            if (data.state == CaptureState.START) {
                logic.startCapture(data.time_ms)
            } else if (data.state == CaptureState.STOP) {
                logic.stopCapture()
            } else {
                logic.finishCapture(data.npc_id)
            }
        } else if (entity.hasComponent(BonusPointTag)) {
            local logic = BonusPointLogic(entity)
            if (data.state == CaptureState.START) {
                logic.startCapture(data.time_ms)
            } else if (data.state == CaptureState.STOP) {
                logic.stopCapture()
            } else {
                logic.finishCapture(data.npc_id, data.team_tag)
                this._map_controller.updateBonus(data.idx, data.team_tag)
            }
        }
    }

    function onFollowUpdate(data) {
        local entity = this._entitites[data.idx]
        if (entity.hasComponent(FlagTag)) {
            local logic = FlagLogic(entity)
            if (data.npc_id != -1) {
                logic.attach(data.npc_id)
            } else {
                logic.detach(data.position)
            }
        }
    }

    function onDestinationReached(data) {
        local entity = this._entitites[data.idx]
        if (entity.hasComponent(FlagTag)) {
            local logic = FlagLogic(entity)
            logic.destinationReached(data.npc_id, data.team_tag)
        }
    }

    function onTeamUpdate(data) {
        local entity = this._entitites[data.idx]
        if (entity.hasComponent(BonusPointTag)) {
            local logic = BonusPointLogic(entity)
            logic.finishCapture(-1, data.team_tag)

            this._map_controller.updateBonus(data.idx, data.team_tag)
        }
    }

    function onLobbyState(message) {
        this.context.machine.change(
            LobbyState(this.context, message)
        )
    }

    function onFinishedState(message) {
        this._winner_label = WinnerLabel(message.winner_team)
        this._winner_label.setVisible(true)

        sounds.finish_game.play()
    }
}
