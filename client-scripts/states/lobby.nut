class LobbyState extends State {
    events = [
        {name = "onKey", handler = "onKey"},
        {name = "state:lobby", handler = "onStateUpdate"},
        {name = "state:gameplay", handler = "onGameplayState"},
    ]

    _board = null
    _timer = null
    _request_team_ts = 0
    _seconds = 0

    constructor(context, data) {
        base.constructor(context)
        this._seconds = data.seconds

        this._board = BoardLobby()
        this._board.init()
        this.onStateUpdate(data)

        local size = this._board.getSize()
        this._board.setPosition(8192 - size.width, 4096 - size.height / 2)
    }

    /////////////////////////////////////////
    ///	Events
    /////////////////////////////////////////

    function onEnter() {
        this._board.setVisible(true)

        // Change game status
        setFreeze(true)
        setHudMode(HUD_ALL, HUD_MODE_HIDDEN)

        // Setup camera
        Camera.movementEnabled = false
        Camera.setPosition(30515.757813, 6513.704102, -21013.550781)
        Camera.setRotation(25, 172, 0)
        
        Chat.print(0, 255, 0, "To change team use: F8")

        // Clock timer
        this._timer = setTimer(this.onTimer.bindenv(this), 1000, 0)

        Discord.activity.state = format("In lobby: %s", getPlayerName(heroId))
        Discord.activity.timestamps.start = 0
        Discord.activity.timestamps.end = time() + this._seconds + 1
        Discord.activity.update()
    }

    function onExit() {
        this._board.setVisible(false)

        // Change game status
        setFreeze(false)
        setHudMode(HUD_ALL, HUD_MODE_DEFAULT)

        // Setup camera
        Camera.movementEnabled = true

        // Clean timer
        killTimer(this._timer)
    }

    function onTimer() {
        this._board.updateTimer(--this._seconds)
    }

    function onKey(key) {
        local current_ts = getTickCount()

        // Rate limitting
        if (key == KEY_F8 && this._request_team_ts < current_ts) {
            local packet = RequestTeamChangeMessage().toPacket()
            packet.send(RELIABLE)
 
            this._request_team_ts = current_ts + 250
        }
    }

    function onStateUpdate(data) {
        if (data.seconds != null) {
            this._board.updateTimer(data.seconds)
            this._seconds = data.seconds

            Discord.activity.timestamps.end = time() + this._seconds + 1
            Discord.activity.update()
        }

        if (data.map != null) {
            local definition = map_definitions[data.map]
            this._board.updateMap(definition.data.name)
        }

        if (data.team != null) {
            this._board.updateTeam(data.team)
        }

        if (data.red_players != null) {
            this._board.updatePlayers(TeamTag.RED, data.red_players)
        }

        if (data.blue_players != null) {
            this._board.updatePlayers(TeamTag.BLUE, data.blue_players)
        }
    }

    function onGameplayState(message) {
        this.context.machine.change(
            GameplayState(this.context, message)
        )
    }
}