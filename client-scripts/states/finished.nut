class FinishedState extends State {
    events = [
        {name = "state:lobby", handler = "onLobbyState"},
    ]

    label = null

    constructor(context) {
        base.constructor(context)

        this.label = Draw(0, 0, "Waiting to launch lobby...")
        this.label.font = "FONT_OLD_20_WHITE_HI.TGA"
        this.label.setPosition(4096 - this.label.width / 2, 4096 - this.label.height / 2)
    }

    /////////////////////////////////////////
    ///	Events
    /////////////////////////////////////////

    function onEnter() {
        this.label.visible = true

        // Change game status
        setFreeze(true)
        setHudMode(HUD_ALL, HUD_MODE_HIDDEN)
        clearMultiplayerMessages()

        // Setup camera
        Camera.movementEnabled = false
        Camera.setPosition(2500, 3000, 0)
        Camera.setRotation(25.0, 90.6825, 0)
    }

    function onExit() {
        this.label.visible = false

        // Change game status
        setFreeze(false)
        setHudMode(HUD_ALL, HUD_MODE_DEFAULT)

        // Setup camera
        Camera.movementEnabled = true
    }

    function onLobbyState(message) {
        this.context.machine.change(
            LobbyState(this.context, message)
        )
    }
}
