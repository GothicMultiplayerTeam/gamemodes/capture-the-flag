class DeadTimer {
    label = null
    _timer = -1
    _seconds = 0

    constructor() {
        this.label = DeadLabel()
    }

    function execute() {
        if (--this._seconds >= 0) {
            this.label.setRespawnTimeLeft(this._seconds)
        }
    }

    function start(seconds) {
        this._seconds = seconds
        this.label.setRespawnTimeLeft(seconds)
        setTimer(this.execute.bindenv(this), 1000, seconds)
    }

    function stop() {
        if (this._timer != -1) {
            killTimer(this._timer)
            this._timer = -1
        }
    }
}