class Marker {
    function setVisible(toggle) {
        throw "Not implemented!"
    }

    function setColor(r, g, b) {
        throw "Not implemented!"
    }

    function setPosition(x, y) {
        throw "Not implemented!"
    }
}

class PlayerMarker extends Marker {
    _marker = null

    constructor() {
        this._marker = Draw(0, 0, "+")
	    this._marker.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._marker.setScale(1, 1)
    }

    function setVisible(toggle) {
        this._marker.visible = toggle
    }

    function setPosition(x, y) {
        this._marker.setPosition(x - this._marker.width / 2, y - this._marker.height / 2)
    }
}

class BaseMarker extends Marker {
    _marker = null

    constructor(team_tag) {
        local color = get_team_color(team_tag)

        this._marker = Draw(0, 0, "X")
	    this._marker.font = "FONT_OLD_20_WHITE_HI.TGA"
        this._marker.setScale(1.5, 1.5)
        this._marker.setColor(color.r, color.g, color.b)
    }

    function setVisible(toggle) {
        this._marker.visible = toggle
    }

    function setPosition(x, y) {
        this._marker.setPosition(x - this._marker.width / 2, y - this._marker.height / 2)
    }
}

class FlagMarker extends Marker {
    _marker = null

    constructor(team_tag) {
        local color = get_team_color(team_tag)

        this._marker = Texture(0, 0, 150, 150 "U.TGA")
        this._marker.setColor(color.r, color.g, color.b)
    }

    function setVisible(toggle) {
        this._marker.visible = toggle
    }

    function setPosition(x, y) {
        this._marker.setPosition(x - 75, y - 150)
    }
}

class BonusMarker extends Marker {
    _marker = null
    _text = null

    constructor(bonus_type) {
        this._marker = Texture(0, 0, 150, 150 "O.TGA")
        this._text = Draw(0, 0, this._getName(bonus_type))
        this._text.setScale(0.80, 0.80)
    }

    function _getName(bonus_type) {
        switch (bonus_type) {
        case BonusType.DAMAGE: return "Damage"
        case BonusType.SPEED: return "Speed"
        case BonusType.REGENERATION: return "Regeneration"
        }

        return ""
    }

    function setVisible(toggle) {
        this._marker.visible = toggle
        this._text.visible = toggle
    }

    function setColor(r, g, b) {
       this._marker.setColor(r, g, b)
    }
    
    function setPosition(x, y) {
        this._marker.setPosition(x - 75, y - 75)
        this._text.setPosition(x - this._text.width / 2, y + 75)
    }
}

class MapDocument extends Texture {
    _elements = null
    _screen = null

    constructor(texture, coords) {
        base.constructor(0, 0, 8192, 8192, texture)

        this._elements = []
        this._screen = ScreenSpace(
            this.getSize(), MinMaxBounds(coords[0], coords[1], coords[2], coords[3])
        )
    }

    function setVisible(toggle) {
        this.visible = toggle

        foreach (element in this._elements) {
            element.setVisible(toggle)
        }
    }

    function insert(marker, x, z) {
        local screen = this._screen.convertTo2d(x, z)
        marker.setPosition(screen.x, screen.y)

        this._elements.push(marker)
        return marker
    }

    function update(marker, x, z) {
        local screen = this._screen.convertTo2d(x, z)
        marker.setPosition(screen.x, screen.y)
    }
}