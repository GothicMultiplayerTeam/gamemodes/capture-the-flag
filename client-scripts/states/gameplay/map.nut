class MapController {
    _document = null
    _player_marker = null
    _markers = null

    constructor(data) {
        this._document = this._createDocument(data.map, data.teams, data.bonuses)
        this._player_marker = this._document.insert(PlayerMarker(), 0, 0)
        this._markers = []
    }

    static function create(instance) {
        if (!(instance in map_definitions)) {
            throw "Map instance '" + instance + "' does not exist!"
        }

        local data = map_definitions[instance].data
        return data.map ? this(data) : null
    }

    function _reallocate(idx) {
        if (idx >= this._markers.len()) {
            this._markers.resize(idx + 1)
        }
    }

    function _createDocument(map, teams, bonuses) {
        local document = MapDocument(map.texture, map.level_coords)
        foreach (tag, team in teams) {
            document.insert(BaseMarker(tag), team.flag.x, team.flag.z)
        }

        return document
    }

    function toggle() {
        this._document.setVisible(!this._document.visible)
    }

    function spawnFlag(idx, team_tag, x, z) {
        this._reallocate(idx)
        this._markers[idx] = this._document.insert(FlagMarker(team_tag), x, z)
    }

    function spawnBonus(idx, bonus, x, z) {
        this._reallocate(idx)
        this._markers[idx] = this._document.insert(BonusMarker(bonus.type), x, z)
    }

    function updateFlag(idx, x, z) {
        local marker = this._markers[idx]
        if (marker) {
            this._document.update(marker, x, z)
        }
    }

    function updateBonus(idx, team_tag) {
        local marker = this._markers[idx]
        if (marker) {
            local color = get_team_color(team_tag)
            marker.setColor(color.r, color.g, color.b)
        }
    }

    function update(entities) {
        if (this._document.visible) {
            local position = getPlayerPosition(heroId)
            this._document.update(this._player_marker, position.x, position.z)

            foreach (entity in entities) {
                if (entity.hasComponent(FlagTag)) {
                    local position = entity.getComponent(Position)
                    this.updateFlag(entity.idx, position.x, position.z)
                }
            }
        }
    }
}