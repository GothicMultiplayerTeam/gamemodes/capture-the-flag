class Map {
    name = null
    _flag_platforms = null
    _banner_platforms = null
    _spawn_platforms = null

    constructor(data) {
        this.name = data.name
        this._flag_platforms = this._createFlagPlatforms(data.teams)
        this._banner_platforms = this._createBonusesPlatforms(data.bonuses)
        this._spawn_platforms = this._createSpawnPlatforms(data.teams)
    }

    static function create(instance) {
        if (!(instance in map_definitions)) {
            throw "Map instance '" + instance + "' does not exist!"
        }

        return this(map_definitions[instance].data)
    }

    function _createFlagPlatforms(teams) {
        local result = []
        foreach (tag, data in teams) {
            local vob = Vob(PLATFORM_FLAG_VISUAL)
            vob.setPosition(data.flag.x, data.flag.y - 650, data.flag.z)
            vob.cdDynamic = true
            vob.addToWorld()

            result.push(vob)
        }

        return result
    }

    function _createBonusesPlatforms(bonuses) {
        local result = []
        foreach (data in bonuses) {
            local visual = null
            switch (data.bonus) {
            case BonusType.DAMAGE: visual = PLATFORM_DAMAGE_VISUAL; break
            case BonusType.SPEED: visual = PLATFORM_SPEED_VISUAL; break
            case BonusType.REGENERATION: visual = PLATFORM_REGENERATION_VISUAL; break
            }

            local vob = Vob(visual)
            vob.setPosition(data.x - 20, data.y - 180, data.z)
            vob.cdDynamic = true
            vob.addToWorld()

            result.push(vob)
        }

        return result
    }

    function _createSpawnPlatforms(teams) {
        local result = []
        foreach (team in teams) {
            local vob = Vob(PLATFORM_SPAWN_VISUAL)
            vob.setPosition(team.spawn.x, team.spawn.y - 100, team.spawn.z)
            vob.cdDynamic = true
            vob.addToWorld()

            result.push(vob)
        }

        return result
    }
}