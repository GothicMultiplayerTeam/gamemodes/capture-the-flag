sounds <- {
    start_game = Sound("OPENPORTAL.WAV"),
    finish_game = Sound("CHAPTER_01.WAV"),
    point = Sound("LEVELUP.WAV"),
    flag_capture = Sound("THRILLJINGLE_03.WAV"),
    dead = [
        Sound("SVM_11_DEAD.WAV"),
        Sound("SVM_12_DEAD.WAV"),
        Sound("SVM_13_DEAD.WAV"),
        Sound("SVM_14_DEAD.WAV"),
        Sound("SVM_15_DEAD.WAV"),
        Sound("SVM_16_DEAD.WAV"),
        Sound("SVM_17_DEAD.WAV"),
        Sound("SVM_18_DEAD.WAV"),
        Sound("SVM_19_DEAD.WAV"),
    ]
}