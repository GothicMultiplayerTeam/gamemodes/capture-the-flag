behaviour <- {
    function rotate_vob(entity, timer) {
        local vob = entity.getComponent(Vob)
        vob.setRotation(0, (timer.elapsed_ms * 0.05) % 359, 0)
    }

    function move_up_down_vob(entity, timer) {
        local vob = entity.getComponent(Vob)
        local position = entity.getComponent(Position)

        vob.setPosition(position.x, position.y + sin(timer.elapsed_ms * 0.005) * 40.0, position.z)
    }
}
