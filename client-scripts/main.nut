local entities = []
local machine = StateMachine()

local context = {
    machine = machine
}

local function init_handler() {
    enableEvent_Render(true)
    enableEvent_RenderFocus(true)
    enable_WeaponTrail(true)
    enable_DamageAnims(false)
    
    Daedalus.constant("RANGED_CHANCE_MINDIST", 10000.0)
    Daedalus.constant("RANGED_CHANCE_MAXDIST", 10000.0)

    machine.push(LoadingState(context))


    // Stany:
    // - �adowanie
    // - Wyb�r gracza
    // - Lobby

    // Serwer trzyma stan gry
    // - pozycja flag

    // GameState
    // red_team: points
    // blue_team: points
    // flags: []
    // banners: []
}

addEventHandler("onInit", init_handler)