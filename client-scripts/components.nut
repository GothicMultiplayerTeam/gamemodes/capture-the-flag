class Position {
    x = 0.0
    y = 0.0
    z = 0.0

    constructor(x, y, z) {
        this.x = x, this.y = y, this.z = z
    }

    function update(position) {
        this.x = position.x
        this.y = position.y
        this.z = position.z
    }
}

class Follow {
    npc_id = -1
    offset = 0

    constructor(npc_id, offset = 0) {
        this.npc_id = npc_id
        this.offset = offset
    }
}

class Behaviours {
    entries = null

    constructor(entries) {
        this.entries = entries
    }
}

class Timer {
    text = null
    time_left_ms = 0
    _last_time_ms = 0

    constructor(text, time_ms) {
        this.text = text
        this.time_left_ms = time_ms + 1000
    }
}

// Transition
// + x, y, z

// Components
// + Lifetime (d�ugo�� �ycia)

// Systems
// + HealingSystem
// + CombatSystem
// + AnimationSystem