function calculate_team_players(team, players) {
    local counter = 0
    foreach (player in players) {
        if (player.team && player.team.tag == team) {
            ++counter
        }
    }

    return counter
}