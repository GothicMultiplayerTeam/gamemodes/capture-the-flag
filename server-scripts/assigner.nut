class TeamAssigner {
    _players = null
    _map = null

    constructor(players, map) {
        this._players = players
        this._map = map
    }

    function _collect_without_team() {
        local result = []
        foreach (player in this._players) {
            if (player.team == null) {
                result.push(player)
            }
        }

        return result
    }

    function _shuffle(players) {
        local shuffled = []
        local length = players.len()

        while (length > 0) {
            local index = random_int(0, length - 1)
            shuffled.push(players[index])

            players.remove(index)
            length--
        }

        return shuffled
    }

    function assign() {
        local total_count = this._players.len()
        local team_members = (total_count / 2).tointeger()
        local red_count = calculate_team_players(TeamTag.RED, this._players)
        local blue_count = calculate_team_players(TeamTag.BLUE, this._players)
        
        local players = this._collect_without_team()
        local shuffled = this._shuffle(players)

        foreach (player in shuffled) {
            if (red_count < team_members) {
                player.setTeam(this._map.getTeam(TeamTag.RED))
                red_count++
            } else {
                player.setTeam(this._map.getTeam(TeamTag.BLUE))
                blue_count++
            }
        }
    }
}