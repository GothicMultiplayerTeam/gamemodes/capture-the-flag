class Statistics {
    kills = 0
    deaths = 0
    assists = 0
}

class Player {
    id = -1
    name = null
    level = 1
    experience = 0
    team = null
    attached = null
    stats = null

    constructor(id) {
        this.id = id
        this.name = getPlayerName(id)
        this.stats = Statistics()
    }

    function reset() {
        // Reset name back to original
        setPlayerName(this.id, this.name)
        this.setTeam(null)
        
        this.level = 1
        this.attached = null
        this.stats = Statistics()
    }

    function setLevel(level) {
        this.level = level
        setPlayerName(this.id, format("Lv %d. %s", this.level, this.name))
    }

    function setTeam(team) {
        this.team = team

        if (team == null) {
            setPlayerColor(this.id, 255, 255, 255)
        } else if (team.tag == TeamTag.RED) {
            setPlayerColor(this.id, 255, 0, 0)
        } else if (team.tag == TeamTag.BLUE) {
            setPlayerColor(this.id, 0, 150, 255)
        }
    }
}

players <- {
    _list = array(getMaxSlots()),
    online = []

    function get(pid) {
        return this._list[pid]
    }
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

local function player_join(pid) {
    local player = Player(pid)

    players._list[pid] = player
    players.online.push(player)
}

addEventHandler("onPlayerJoin", player_join, 1)

//----------------------------------------

local function player_disconnect(pid, reason) {
    players._list[pid] = null

    local len = players.online.len()
    for (local i = 0; i < len; ++i) {
        if (players.online[i].id != pid)
            continue

        players.online[i] = players.online[len - 1]
        players.online.pop()
        return
    }
}

addEventHandler("onPlayerDisconnect", player_disconnect, 999999)
