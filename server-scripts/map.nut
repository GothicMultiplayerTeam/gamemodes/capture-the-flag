class Map {
    instance = null
    world = null
    scene = null
    teams = null

    constructor(instance, data) {
        this.instance = instance
        this.world = data.world
        this.scene = Scene()
        this.teams = this._createTeams(data.teams)

        this._createBonusPoints(data.bonuses)
        this._createFlags()
    }

    static function create(instance) {
        if (!(instance in map_definitions)) {
            throw "Map instance '" + instance + "' does not exist!"
        }

        return this(instance, map_definitions[instance].data)
    }

    function _createTeams(teams) {
        local result = {}
        foreach (tag, data in teams) {
            local team = Team(tag)
            team.base_position = Position(data.flag.x, data.flag.y, data.flag.z)
            team.spawn_position = Position(data.spawn.x, data.spawn.y, data.spawn.z)
            team.spawn_angle = data.spawn.angle
            team.inventory = data.inventory

            result[tag] <- team
        }

        return result
    }

    function _createFlags() {
        foreach (tag, team in this.teams) {
            local position = team.base_position
            factory.createFlag(this.scene, team, position.x, position.y, position.z)
        }
    }

    function _createBonusPoints(bonuses) {
        foreach (data in bonuses) {
            factory.createBonusPoint(this.scene, data.bonus, data.factor, data.x, data.y, data.z)
        }
    }

    function getTeam(tag) {
        return tag in this.teams ? this.teams[tag] : null
    }

    function getClasses(tag) {
        return tag in this._classes ? this._classes[tag] : null
    }
}
