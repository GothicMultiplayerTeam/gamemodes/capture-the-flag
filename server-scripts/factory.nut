factory <- {
    function createFlag(scene, team, x, y, z) {
        local entity = scene.create()
        entity.addComponent(FlagTag())
        entity.addComponent(team)
        entity.addComponent(Position(x, y, z))
        entity.addComponent(CollisionCircular(CAPTURE_FLAG_RADIUS_M))
        entity.addComponent(Capture(CAPTURE_FLAG_TIME_SEC * 1000))

        return entity
    }

    function createBonusPoint(scene, type, factor, x, y, z) {
        local entity = scene.create()
        entity.addComponent(BonusPointTag())
        entity.addComponent(Position(x, y, z))
        entity.addComponent(Bonus(type, factor))
        entity.addComponent(CollisionCircular(CAPTURE_BONUS_RADIUS_M))
        entity.addComponent(Capture(CAPTURE_BONUS_TIME_SEC * 1000))

        return entity
    }
}
