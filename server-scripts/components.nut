class Position {
    x = 0.0
    y = 0.0
    z = 0.0

    constructor(x, y, z) {
        this.x = x, this.y = y, this.z = z
    }

    function update(position) {
        this.x = position.x
        this.y = position.y
        this.z = position.z
    }
}

class Team {
    tag = -1
    base_position = null
    spawn_position = null
    spawn_angle = 0
    inventory = null
    points = 0
    bonuses = null

    constructor(tag) {
        this.tag = tag
        this.inventory = []
        this.bonuses = {
            damage_factor = 1.0,
            speed_factor = 1.0,
            regenerate_factor = 1.0
        }
    }
}

class Follow {
    npc_id = -1

    constructor(npc_id) {
        this.npc_id = npc_id
    }
}

class CollisionCircular {
    radius = 0.0
    nearest_pid = -1
    collides_with = null

    constructor(radius) {
        this.radius = radius
        this.collides_with = []
    }
}

class Capture {
    time_ms = 0
    total_time_ms = 0
    npc_id = -1

    constructor(time_ms) {
        this.time_ms = time_ms
        this.total_time_ms = time_ms
    }
}

class ReachDestination {
    position = null
    radius = 0

    constructor(position, radius) {
        this.position = position
        this.radius = radius
    }
}

class InvokeAfter {
    callback = null
    time_left_ms = 0

    constructor(callback, time_ms) {
        this.callback = callback
        this.time_left_ms = time_ms
    }
}

class Bonus {
    type = -1
    factor = 1.0

    constructor(type, factor) {
        this.type = type
        this.factor = factor
    }
}

// Components
// + Lifetime (d�ugo�� �ycia)

// Systems
// + HealingSystem
// + CombatSystem
// + AnimationSystem
