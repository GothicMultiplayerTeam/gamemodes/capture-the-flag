// Custom events
addEvent("capture:started")
addEvent("capture:stopped")
addEvent("capture:finished")

//---------------------------------------

local function is_player_valid(pid) {
    return isPlayerSpawned(pid) && !isPlayerDead(pid)
}

function capture_system(entities, timestep) {
    foreach (entity in entities) {
        local capture = entity.getComponent(Capture)
        local circular = entity.getComponent(CollisionCircular)

        if (capture == null || circular == null) {
            continue
        }

        if (capture.npc_id == -1) {
            capture.npc_id = circular.nearest_pid
            if (capture.npc_id != -1 && is_player_valid(capture.npc_id)) {
                callEvent("capture:started", capture.npc_id, entity)
            }
        } else if (is_player_valid(capture.npc_id) && circular.collides_with.find(capture.npc_id) != null) {
            capture.time_ms -= timestep.delta_ms
            if (capture.time_ms <= 0) {
                entity.removeComponent(Capture)
                callEvent("capture:finished", capture.npc_id, entity)
            }
        } else {
            callEvent("capture:stopped", capture.npc_id, entity)

            // Reset values
            capture.npc_id = -1
            capture.time_ms = capture.total_time_ms
        }
    }
}
