// Custom events
addEvent("collision:detected")

//---------------------------------------

function collision_system(entites) {
    foreach (entity in entites) {
        local circular = entity.getComponent(CollisionCircular)
        local position = entity.getComponent(Position)

        if (circular == null || position == null) {
            continue
        }

        // Clear before lookup
        circular.nearest_pid = -1
        circular.collides_with.clear()

        local last_distance = 9999999.0
        foreach (player in players.online) {
            if (isPlayerSpawned(player.id)) {
                local target = getPlayerPosition(player.id)
                local distance = getDistance3d(target.x, target.y, target.z, position.x, position.y, position.z)

                // You can skip some players from collision if u want
                if (distance <= circular.radius && !callEvent("collision:detected", player.id, entity)) {
                    circular.collides_with.push(player.id)

                    if (distance < last_distance) {
                        last_distance = distance
                        circular.nearest_pid = player.id
                    }
                }
            }
        }
    }
}
