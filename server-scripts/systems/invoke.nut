function invoke_system(entities, timestep) {
    foreach (entity in entities) {
        local invoke = entity.getComponent(InvokeAfter)
        if (invoke == null) {
            continue
        }

        invoke.time_left_ms -= timestep.delta_ms
        if (invoke.time_left_ms <= 0) {
            entity.removeComponent(InvokeAfter)
            invoke.callback(entity)
        }
    }
}