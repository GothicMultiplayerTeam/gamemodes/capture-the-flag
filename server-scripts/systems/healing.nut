local next_ts = 0

function healing_system(players, timestep) {
    if (next_ts < timestep.elapsed_ms) {
        foreach (player in players) {
            if (!isPlayerDead(player.id)) {
                local spawn = player.team.spawn_position
                local position = getPlayerPosition(player.id)

                local distance = getDistance3d(spawn.x, spawn.y, spawn.z, position.x, position.y, position.z)
                if (distance <= RESPAWN_RADIUS_M) {
                    local max_health = getPlayerMaxHealth(player.id)
                    local health = getPlayerHealth(player.id)

                    local new_health = health + max_health * HEALING_FACTOR
                    if (new_health > max_health) {
                        new_health = max_health
                    }

                    if (health != new_health) {
                        setPlayerHealth(player.id, new_health.tointeger())
                    }
                }
            }
        }

        next_ts += HEALING_TIME_SEC * 1000
    }
}