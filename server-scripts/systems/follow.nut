function follow_system(entites) {
    foreach (entity in entites) {
        local follow = entity.getComponent(Follow)
        local position = entity.getComponent(Position)

        if (follow == null || position == null) {
            continue
        }

        local player_position = getPlayerPosition(follow.npc_id)
        position.x = player_position.x
        position.y = player_position.y
        position.z = player_position.z
    }
}
