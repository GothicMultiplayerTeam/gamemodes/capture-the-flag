// Custom events
addEvent("destination:reached")

//---------------------------------------

function destination_system(entites) {
    foreach (entity in entites) {
        local destination = entity.getComponent(ReachDestination)
        local position = entity.getComponent(Position)

        if (destination == null || position == null) {
            continue
        }

        local distance = getDistance3d(
            position.x, position.y, position.z,
            destination.position.x, destination.position.y, destination.position.z
        )

        if (distance <= destination.radius) {
            entity.removeComponent(ReachDestination)
            callEvent("destination:reached", entity)
        }
    }
}
