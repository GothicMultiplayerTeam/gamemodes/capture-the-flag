class ACP {
    _state = null
    _commands = null

    constructor(state) {
        this._state = state
        this._commands = {}
    }

    function register(cmd, callback) {
        this._commands[cmd] <- callback
    }

    function handle(pid, cmd, params) {
        if (!checkPermission(pid, LEVEL.MOD))
            return

        if (cmd in this._commands) {
            this._commands[cmd].call(this, pid, params)
        }
    }
}
