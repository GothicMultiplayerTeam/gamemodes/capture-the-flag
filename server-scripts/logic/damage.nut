function damage_calculator(player, killer) {
    local difference = player.level - killer.level
    if (difference > 0) difference = 0

    local damage = ceil(getPlayerMaxHealth(player.id).tofloat() / (MAX_LEVEL + difference * DAMAGE_INCREASE_FACTOR))
    return ceil(damage * killer.team.bonuses.damage_factor).tointeger()
}
