addEvent("flag:restored")
addEvent("flag:captured")
addEvent("flag:point")

//--------------------------------------

class FlagLogic {
    _entity = null

    constructor(entity) {
        this._entity = entity
    }

    function isInBase() {
        local team = this._entity.getComponent(Team)
        local position = this._entity.getComponent(Position)

        return (
            position.x == team.base_position.x &&
            position.y == team.base_position.y &&
            position.z == team.base_position.z
        )
    }

    function canTake(player) {
        local team = this._entity.getComponent(Team)

        // In base only enemy, outsite everybody ;)
        if (this.isInBase()) {
            return player.team != null && player.team.tag != team.tag && !isPlayerDead(player.id)
        } else {
            return player.team != null && !isPlayerDead(player.id)
        }
    }

    function moveToBase() {
        local team = this._entity.getComponent(Team)
        local position = this._entity.getComponent(Position)

        // Reset positon back to base
        position.update(team.base_position)
    }

    function attach(player) {
        this._entity.removeComponent(CollisionCircular)
        this._entity.addComponent(Follow(player.id))
        this._entity.addComponent(ReachDestination(player.team.base_position, CAPTURE_FLAG_RADIUS_M * 1.5))

        // Attach to player
        player.attached = this._entity
    }

    function detach(player) {
        // If follow was removed due disconnection etc.
        if (this._entity.hasComponent(Follow)) {
            this._entity.removeComponent(Follow)
        }
        
        if (this._entity.hasComponent(ReachDestination)) {
            this._entity.removeComponent(ReachDestination)
        }

        if (!this._entity.hasComponent(CollisionCircular)) {
            this._entity.addComponent(CollisionCircular(CAPTURE_FLAG_RADIUS_M))
        }
        
        this._entity.addComponent(Capture(CAPTURE_FLAG_TIME_SEC * 1000))
 
        // Detach from player
        player.attached = null
    }
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

local function collision_detected(pid, entity) {
    if (entity.hasComponent(FlagTag)) {
        local player = players.get(pid)
        local logic = FlagLogic(entity)

        if (!logic.canTake(player)) {
            cancelEvent()
        }
    }
}

addEventHandler("collision:detected", collision_detected)

local function capture_started(pid, entity) {
    if (entity.hasComponent(FlagTag)) {
        local player = players.get(pid)
        local capture = entity.getComponent(Capture)

        local message = CaptureStateMessage(entity.idx, CaptureState.START, pid, player.team.tag)
        message.time_ms = capture.time_ms

        local packet = message.toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

addEventHandler("capture:started", capture_started)

local function capture_stopped(pid, entity) {
    if (entity.hasComponent(FlagTag)) {
        local player = players.get(pid)
        local packet = CaptureStateMessage(entity.idx, CaptureState.STOP, pid, player.team.tag).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

addEventHandler("capture:stopped", capture_stopped)

local function capture_finished(pid, entity) {
    if (entity.hasComponent(FlagTag)) {
        local player = players.get(pid)
        local logic = FlagLogic(entity)
        local team = entity.getComponent(Team)

        local message = FollowStateMessage(entity.idx, -1)
        if (player.team.tag == team.tag) {
            logic.detach(player)
            logic.moveToBase()
            message.position = entity.getComponent(Position)

            callEvent("flag:restored", pid)
        } else {
            logic.attach(player)
            message.npc_id = pid

            callEvent("flag:captured", pid)
        }

        local packet = CaptureStateMessage(entity.idx, CaptureState.FINISH, pid, player.team.tag).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }

        local packet = message.toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

addEventHandler("capture:finished", capture_finished)

local function destination_reached(entity) {
    if (entity.hasComponent(FlagTag)) {
        local follow = entity.getComponent(Follow)
        local position = entity.getComponent(Position)
        local player = players.get(follow.npc_id)

        local logic = FlagLogic(entity)
        logic.detach(player)
        logic.moveToBase()

        local packet = DestinationStateMessage(entity.idx, player.team.tag, follow.npc_id).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE)
        }

        local packet = FollowStateMessage(entity.idx, -1, position).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }

        callEvent("flag:point", follow.npc_id)
    }
}

addEventHandler("destination:reached", destination_reached)

local function drop_flag(pid) {
    local player = players.get(pid)
    local entity = player.attached

    if (player && entity && entity.hasComponent(FlagTag)) {
        local logic = FlagLogic(entity)
        logic.detach(player)

        local position = entity.getComponent(Position)
        local packet = FollowStateMessage(entity.idx, -1, position).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

local function player_hit(pid, kid, dmg, type) {
    drop_flag(pid)
}


local function player_dead(pid, kid) {
    drop_flag(pid)
}

local function player_disconnect(pid, reason) {
    drop_flag(pid)
}

addEventHandler("onPlayerHit", player_hit)
addEventHandler("onPlayerDead", player_dead)
addEventHandler("onPlayerDisconnect", player_disconnect)
