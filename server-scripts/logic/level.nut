function calculate_next_experience(level) {
    local total = EXPERIENCE_BASE
    for (local i = 1; i < level; ++i) {
        total += EXPERIENCE_FACTOR * (i - 1)
    }

    return total
}

function give_experience(player, experience) {
    if (player.level < MAX_LEVEL) {
        player.experience += experience
        if (player.experience >= calculate_next_experience(player.level + 1)) {
            player.setLevel(player.level + 1)
        }
    }
}
