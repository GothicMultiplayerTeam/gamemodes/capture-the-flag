addEvent("bonus:damage")
addEvent("bonus:speed")

//--------------------------------------

local bonus_expired

class BonusPointLogic {
    _entity = null

    constructor(entity) {
        this._entity = entity
    }

    function _bonusDamage(factor, apply) {
        local team = this._entity.getComponent(Team)
        team.bonuses.damage_factor = factor

        callEvent("bonus:damage", team.tag, apply)
    }

    function _bonusSpeed(factor, apply) {
        local team = this._entity.getComponent(Team)
        team.bonuses.speed_factor = factor

        callEvent("bonus:speed", team.tag, apply)
    }

    function canTake(player) {
        return !this._entity.hasComponent(Team)
    }

    function expire() {
        local bonus = this._entity.getComponent(Bonus)
        if (bonus) {
            switch (bonus.type) {
            case BonusType.DAMAGE: this._bonusDamage(1.0, false); break
            case BonusType.SPEED: this._bonusSpeed(1.0, false); break
            case BonusType.REGENERATION: break
            }
        }

        this._entity.removeComponent(Team)
        this._entity.addComponent(CollisionCircular(CAPTURE_BONUS_RADIUS_M))
        this._entity.addComponent(Capture(CAPTURE_BONUS_TIME_SEC * 1000))
    }

    function captured(player) {
        this._entity.removeComponent(CollisionCircular)
        this._entity.addComponent(player.team)
        this._entity.addComponent(InvokeAfter(bonus_expired, BONUS_TIME_SEC * 1000))

        local bonus = this._entity.getComponent(Bonus)
        if (bonus) {
            switch (bonus.type) {
            case BonusType.DAMAGE: this._bonusDamage(bonus.factor, true); break
            case BonusType.SPEED: this._bonusSpeed(bonus.factor, true); break
            case BonusType.REGENERATION: break
            }
        }
    }
}

/////////////////////////////////////////
///	Events
/////////////////////////////////////////

bonus_expired = function (entity) {
    local logic = BonusPointLogic(entity)
    logic.expire()

    local packet = TeamStateMessage(entity.idx, TeamTag.NONE).toPacket()
    foreach (player in players.online) {
        packet.send(player.id, RELIABLE_ORDERED)
    }
}

local function collision_detected(pid, entity) {
    if (entity.hasComponent(BonusPointTag)) {
        local player = players.get(pid)
        local logic = BonusPointLogic(entity)

        if (!logic.canTake(player)) {
            cancelEvent()
        }
    }
}

addEventHandler("collision:detected", collision_detected)

local function capture_started(pid, entity) {
    if (entity.hasComponent(BonusPointTag)) {
        local player = players.get(pid)
        local capture = entity.getComponent(Capture)

        local message = CaptureStateMessage(entity.idx, CaptureState.START, pid, player.team.tag)
        message.time_ms = capture.time_ms

        local packet = message.toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

addEventHandler("capture:started", capture_started)

local function capture_stopped(pid, entity) {
    if (entity.hasComponent(BonusPointTag)) {
        local player = players.get(pid)
        local packet = CaptureStateMessage(entity.idx, CaptureState.STOP, pid, player.team.tag).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

addEventHandler("capture:stopped", capture_stopped)

local function capture_finished(pid, entity) {
    if (entity.hasComponent(BonusPointTag)) {
        local player = players.get(pid)
        local logic = BonusPointLogic(entity)
        logic.captured(player)

        local packet = CaptureStateMessage(entity.idx, CaptureState.FINISH, pid, player.team.tag).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }
    }
}

addEventHandler("capture:finished", capture_finished)
