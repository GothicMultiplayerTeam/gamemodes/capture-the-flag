class TeamSpawner {
    _players = null

    constructor(players) {
        this._players = players
    }

    function spawn(team, radius) {
        local spawn_point = team.spawn_position

        foreach (player in this._players) {
            if (player.team != team) {
                continue
            }

            local offset_x = random_float(0, radius)
            local offset_z = random_float(0, radius)

            setPlayerPosition(player.id, spawn_point.x + offset_x, spawn_point.y, spawn_point.z + offset_z)
            setPlayerAngle(player.id, team.spawn_angle)
            spawnPlayer(player.id)
        }
    }

    function unspawn(team) {
        foreach (player in this._players) {
            if (player.team != team) {
                continue
            }

            unspawnPlayer(player.id)
        }
    }
}