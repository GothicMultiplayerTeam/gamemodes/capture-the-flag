// Stany
// - Lobby
// - Gra

// Flaga:
// - Osoba kt�ra ma flag� nie mo�e atakowa�
// - Je�li wyci�gnie bro� flaga spada
// - Je�li otrzyma obra�enia flaga spada
// - Przeci�gni�cie flagi do bazy daje punkt

// Przej�cie flagi odblokowuje co�?

// // Opcjonalne
// Dodatkowe atrakcje:
// - Sztandar do przej�cia daj�cy regeneracje HP
// - Sztandar do przej�cia daj�cy bonus damage 5%

// Game:
// - Gra trwa do 5 punkt�w.
// - Wyb�r jednej z 3 klas: Wojownik/Mag/Tank?
// - Zabijanie upgraduje bro�
// - Przej�cie flagi upgraduje bro� calego teamu
// - Bro� spada o poziom po �mierci
// - Ka�dy mo�e do�acza� kiedy chce

// Klasy:
// - Wojownik (podstawa)
// // TO mo�e potem na pocz�tku sam Woj bo ci�ko z balansem mo�e by� xD
// - Tank (wi�cej HP dostaje mniej DMG)
// - Mag (range damge, ma�o HP)

srand(time())

machine <- StateMachine()

local context = {
    machine = machine
}

machine.push(LobbyState(context, LOBBY_TIME_SEC))

local function player_cmd(pid, cmd, params) {
    if (cmd == "pos") {
        local position = getPlayerPosition(pid)
        print("=======")
        print(position.x + ", " + position.y + ", " + position.z)
        print(getPlayerAngle(pid))
    }
}

addEventHandler("onPlayerCommand", player_cmd)
