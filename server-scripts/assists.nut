class AssistsController {
    _matrix = null

    constructor(max_players) {
        this._matrix = array(max_players)
        for (local i = 0; i < max_players; ++i) {
            this._matrix[i] = array(max_players, null)
        }
    }

    function update(kid, pid) {
        this._matrix[pid][kid] = getTickCount()
    }

    function clear(pid) {
        local len = this._matrix[pid].len()
        for (local i = 0; i < len; ++i) {
            this._matrix[pid][i] = null
        }
    }

    function get(pid, timeout = 30000) {
        local players = []
        local check_ts = getTickCount() - timeout

        foreach (id, ts in this._matrix[pid]) {
            if (ts != null && ts >= check_ts) {
                players.push(id)
            }
        }

        return players
    }
}