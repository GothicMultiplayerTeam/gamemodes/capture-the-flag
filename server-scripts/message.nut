function send_message(r, g, b, message) {
    local packet = InfoMessage(r, g, b, message).toPacket()
    foreach (player in players.online) {
        packet.send(player.id, RELIABLE)
    }
}