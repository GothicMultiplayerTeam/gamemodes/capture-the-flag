class LobbyACP extends ACP {
    constructor(state) {
        base.constructor(state)

        // Commands
        this.register("map", this.CmdMap)
        this.register("timeleft", this.CmdTime)
    }

    /////////////////////////////////////////
    ///	Commands
    /////////////////////////////////////////

    function CmdMap(pid, params) {
        local args = sscanf("s", params)
        if (!args) {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Type /map instance")
            return
        }

        local instance = args[0]
        if (instance in map_definitions) {
            this._state.updateMap(instance)
            sendMessageToPlayer(pid, 0, 255, 0, "ACP: Updated map instance!")
        } else {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Invalid map instance!")
        }
    }

    function CmdTime(pid, params) {
        local args = sscanf("d", params)
        if (!args) {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Type /timeleft seconds")
            return
        }

        local seconds = args[0]
        if (seconds > 0) {
            this._state.updateTime(seconds)
            sendMessageToPlayer(pid, 0, 255, 0, "ACP: Updated time left!")
        } else {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Seconds must be positive number!")
        }
    }
}
