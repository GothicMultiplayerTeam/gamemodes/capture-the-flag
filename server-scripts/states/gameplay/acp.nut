class GameplayACP extends ACP {
    constructor(state) {
        base.constructor(state)

        // Commands
        this.register("team", this.CmdTeam)
    }

    /////////////////////////////////////////
    ///	Commands
    /////////////////////////////////////////

    function CmdTeam(pid, params) {
        local args = sscanf("dd", params)
        if (!args) {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Type /team pid team_tag (1|2)")
            return
        }

        local tid = args[0], team_tag = args[1]
        if (!isPlayerSpawned(tid)) {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Given player isn't spawned")
            return
        }

        if (this._state.changeTeam(tid, team_tag)) {
            sendMessageToPlayer(pid, 0, 255, 0, "ACP: Updated " + getPlayerName(tid) + " team!")
            sendMessageToPlayer(pid, 0, 255, 0, "ACP: " + getPlayerName(pid) + " changed your team!")
        } else {
            sendMessageToPlayer(pid, 255, 0, 0, "ACP: Invalid team id!")
        }
    }
}
