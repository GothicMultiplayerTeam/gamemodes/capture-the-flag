class GameplayState extends State {
    events = [
        {name = "onPlayerJoin", handler = "onJoin"},
        {name = "onPlayerHit", handler = "onHit"},
        {name = "onPlayerDead", handler = "onDead"},
        {name = "onPlayerRespawn", handler = "onRespawn"},
        {name = "onPlayerCommand", handler = "onCommand"},
        {name = "onPlayerMessage", handler = "onMessage"},
        {name = "team:change", handler = "onRequestChangeTeam"},
        {name = "flag:restored", handler = "onFlagRestored"},
        {name = "flag:captured", handler = "onFlagCaptured"},
        {name = "flag:point", handler = "onFlagPoint"},
        {name = "bonus:damage", handler = "onBonusDamage"},
        {name = "bonus:speed", handler = "onBonusSpeed"},
    ]

    _acp = null
    _map = null
    _timer = null
    _timestep = null
    _assists = null

    constructor(context, map) {
        base.constructor(context)
        this._acp = GameplayACP(this)
        this._map = map
        this._timestep = Timestep()
        this._assists = AssistsController(getMaxSlots())
    }

    function _respawnPlayer(player) {
        if (player.team) {
            local position = player.team.spawn_position

            setPlayerPosition(player.id, position.x, position.y, position.z)
            setPlayerAngle(player.id, player.team.spawn_angle)
            spawnPlayer(player.id)
        }
    }

    function _setupStatistics(player) {
        if (player.team) {
            // Stats
            setPlayerMaxHealth(player.id, 400)
            setPlayerHealth(player.id, 400)
            setPlayerMaxMana(player.id, 200)
            setPlayerMana(player.id, 200)
            setPlayerStrength(player.id, 200)
            setPlayerDexterity(player.id, 200)
            setPlayerMagicLevel(player.id, 6)
            
            // Weapon skill percent
            setPlayerSkillWeapon(player.id, WEAPON_1H, 100)
            setPlayerSkillWeapon(player.id, WEAPON_2H, 100)
            setPlayerSkillWeapon(player.id, WEAPON_BOW, 100)
            setPlayerSkillWeapon(player.id, WEAPON_CBOW, 100)

            // Level
            player.setLevel(player.level)

            local message = StatisticsMessage()
            message.setLevel(player.level)
            message.setExperience(player.experience)
            message.setNextExperience(calculate_next_experience(player.level + 1))
            message.toPacket().send(player.id, RELIABLE_ORDERED)
        }
    }

    function _setupInventory(player) {
        if (player.team) {
            foreach (item in player.team.inventory) {
                giveItem(player.id, Items.id(item.instance), item.amount)

                if ("equip" in item) {
                    local slot = -1
                    if ("slot" in item) {
                        slot = item.slot
                    }

                    equipItem(player.id, Items.id(item.instance), slot)
                }
            }
        }
    }

    function changeTeam(pid, team_tag) {
        local player = players.get(pid)
        local team = this._map.getTeam(team_tag)

        if (player && team) {
            setPlayerHealth(player.id, 0) // Kill him so he will respawn in new team
            player.setTeam(team)

            return true
        }

        return false
    }

    /////////////////////////////////////////
    ///	Events
    /////////////////////////////////////////
    
    function onEnter() {
        // Notify about new state
        local message = GameplayInitMessage()
        message.setMap(this._map.instance)
        message.insertEntities(this._map.scene.entities)

        local packet = message.toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }

        // Automatically assign team for players without it
        local assigner = TeamAssigner(players.online, this._map)
        assigner.assign()

        local spawner = TeamSpawner(players.online)
        foreach (team in this._map.teams) {
            spawner.spawn(team, RESPAWN_RADIUS_M)
        }

        // Setup respawn times
        foreach (player in players.online) {
            setPlayerRespawnTime(player.id, RESPAWN_TIME_SEC * 1000)
            this._setupStatistics(player)
            this._setupInventory(player)
        }

        // Update logic timer
        this._timer = setTimer(this.onTimer.bindenv(this), 200, 0)
    }

    function onExit() {
        killTimer(this._timer)
    }

    function onTimer() {
        if (players.online.len() == 0) {
            this.context.machine.change(LobbyState(this.context, LOBBY_TIME_SEC))
            return
        }

        this._timestep.update()

        collision_system(this._map.scene.entities)
        capture_system(this._map.scene.entities, this._timestep)
        follow_system(this._map.scene.entities)
        destination_system(this._map.scene.entities)
        invoke_system(this._map.scene.entities, this._timestep)
        healing_system(players.online, this._timestep)
    }

    function onJoin(pid) {
        local team_red = this._map.getTeam(TeamTag.RED)
        local team_blue = this._map.getTeam(TeamTag.BLUE)

        local message = GameplayInitMessage()
        message.setMap(this._map.instance)
        message.setRedScore(team_red.points)
        message.setBlueScore(team_blue.points)
        message.insertEntities(this._map.scene.entities)
        message.toPacket().send(pid, RELIABLE_ORDERED)

        local player = players.get(pid)

        // Automatically assing team, so balance will be maintained
        local assigner = TeamAssigner(players.online, this._map)
        assigner.assign()

        setPlayerRespawnTime(pid, RESPAWN_TIME_SEC * 1000)

        // Spawn in assigned team
        this._respawnPlayer(player)
        this._setupStatistics(player)
        this._setupInventory(player)
    }

    function onHit(pid, kid, dmg, type) {
        if (kid != -1) {
            local player = players.get(pid)
            local killer = players.get(kid)

            // Disable friendly fire
            if (player.team.tag == killer.team.tag) {
                cancelEvent()
            } else {
                this._assists.update(killer.id, player.id)
                eventValue(-damage_calculator(player, killer))
            }
        }
    }

    function onDead(pid, kid) {
        local player = players.get(pid)
        // Increase respawn time
        local respawn_time_sec = (RESPAWN_TIME_SEC + player.stats.deaths * RESPAWN_TIME_INC)
        if (respawn_time_sec > RESPAWN_TIME_MAX) {
            respawn_time_sec = RESPAWN_TIME_MAX
        }

        local message = DeadMessage()
        message.seconds = respawn_time_sec
        message.toPacket().send(player.id, RELIABLE)

        setPlayerRespawnTime(player.id, respawn_time_sec * 1000)
        ++player.stats.deaths

        local message = StatisticsMessage()
        message.setDeaths(player.stats.deaths)
        message.toPacket().send(player.id, RELIABLE_ORDERED)

        if (kid != -1) {
            local assists = this._assists.get(player.id)
            foreach (aid in assists) {
                if (aid != kid) {
                    local assisted_player = players.get(aid)
                    local level = assisted_player.level

                    give_experience(assisted_player, ASSIST_EXPERIENCE)

                    local message = StatisticsMessage()
                    message.setAssists(++assisted_player.stats.assists)
                    message.setExperience(assisted_player.experience)

                    if (level != assisted_player.level) {
                        message.setLevel(assisted_player.level)
                        message.setExperience(calculate_next_experience(assisted_player.level + 1))

                        local color = get_team_color(assisted_player.team.tag)
                        send_message(color.r, color.g, color.b, format("%s reaches level %d", assisted_player.name, assisted_player.level))
                    }

                    message.toPacket().send(assisted_player.id, RELIABLE_ORDERED)
                }
            }

            local killer = players.get(kid)
            local level = killer.level

            give_experience(killer, KILL_EXPERIENCE)
            giveItem(killer.id, Items.id("ITPO_HEALTH_03"), 3)
            giveItem(killer.id, Items.id("ITPO_MANA_03"), 2)

            local color = get_team_color(killer.team.tag)
            send_message(color.r, color.g, color.b, format("%s killed %s", killer.name, player.name))

            local message = StatisticsMessage()
            message.setKills(++killer.stats.kills)
            message.setExperience(killer.experience)

            if (level != killer.level) {
                message.setLevel(killer.level)
                message.setNextExperience(calculate_next_experience(killer.level + 1))

                send_message(color.r, color.g, color.b, format("%s reaches level %d", killer.name, killer.level))
            }

            message.toPacket().send(killer.id, RELIABLE_ORDERED)
            this._assists.clear(pid)
        } else {
            local color = get_team_color(player.team.tag)
            send_message(color.r, color.g, color.b, format("%s killed himself.", player.name))
        }
    }

    function onRespawn(pid) {
        local player = players.get(pid)
        
        this._respawnPlayer(player)
        this._setupStatistics(player)
        this._setupInventory(player)
    }

    function onRequestChangeTeam(pid) {
        
    }

    function onCommand(pid, cmd, params) {
        this._acp.handle(pid, cmd, params)
    }

    function onMessage(pid, message) {
        // Global message
        if (message[0] == '!') {
            message = message.slice(1)
            sendPlayerMessageToAll(pid, 255, 255, 255, "(G) " + message)
        } else {
            local sender = players.get(pid)
            local color = null

            switch (sender.team.tag) {
            case TeamTag.RED: color = {r = 255, g = 150, b = 150}; break
            case TeamTag.BLUE: color = {r = 150, g = 200, b = 255}; break
            }

            foreach (player in players.online) {
                if (sender.team.tag == player.team.tag) {
                    sendPlayerMessageToPlayer(pid, player.id, color.r, color.g, color.b, message)
                }
            }
        }
    }

    function onFlagRestored(pid) {
        local player = players.get(pid)

        switch (player.team.tag) {
        case TeamTag.RED:
            send_message(255, 0, 0, "# The red team has restored flag!")
            break
        
        case TeamTag.BLUE:
            send_message(0, 150, 255, "# The blue team has restored flag!")
            break
        }
    }

    function onFlagCaptured(pid) {
        local player = players.get(pid)

        switch (player.team.tag) {
        case TeamTag.RED:
            send_message(255, 0, 0, "# The red team has stolen flag!")
            break
        
        case TeamTag.BLUE:
            send_message(0, 150, 255, "# The blue team has stolen flag!")
            break
        }
    }

    function onFlagPoint(pid) {
        local player = players.get(pid)
        local message = ScoreMessage()

        switch (player.team.tag) {
        case TeamTag.RED:
            send_message(255, 0, 0, "# The red team scores a point!")
            message.setRedScore(player.team.points + 1)
            break
        
        case TeamTag.BLUE:
            send_message(0, 150, 255, "# The blue team scores a point!")
            message.setBlueScore(player.team.points + 1)
            break
        }

        // Notify about score change
        local packet = message.toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE_ORDERED)
        }

        if (++player.team.points >= MATCH_POINT) {
            this.context.machine.change(FinishedState(this.context, player.team.tag, this._map))
        }
    }

    function onBonusDamage(team_tag, apply) {
        switch (team_tag) {
        case TeamTag.RED:
            if (apply) {
                send_message(255, 0, 0, "@ The red team gained bonus damage.")
            } else {
                send_message(255, 0, 0, "@ The red team lost bonus damage.")
            }
            break
        
        case TeamTag.BLUE:
            if (apply) {
                send_message(0, 150, 255, "@ The blue team gained bonus damage.")
            } else {
                send_message(0, 150, 255, "@ The blue team lost bonus damage.")
            }
            break
        }
    }

    function onBonusSpeed(team_tag, apply) {
        switch (team_tag) {
        case TeamTag.RED:
            if (apply) {
                send_message(255, 0, 0, "@ The red team gained bonus speed.")
            } else {
                send_message(255, 0, 0, "@ The red team lost bonus speed.")
            }
            break
        
        case TeamTag.BLUE:
            if (apply) {
                send_message(0, 150, 255, "@ The blue team gained bonus speed.")
            } else {
                send_message(0, 150, 255, "@ The blue team lost bonus speed.")
            }
            break
        }

        local team = this._map.getTeam(team_tag)
        foreach (player in players.online) {
            if (player.team.tag == team_tag) {
                setPlayerScale(player.id, 1.0, 1.0, team.bonuses.speed_factor)
            }
        }
    }
}