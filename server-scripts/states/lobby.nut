class LobbyState extends State {
    events = [
        {name = "onPlayerJoin", handler = "onJoin"},
        {name = "onPlayerHit", handler = "onHit"},
        {name = "onPlayerCommand", handler = "onCommand"},
        {name = "onPlayerMessage", handler = "onMessage"},
        {name = "team:change", handler = "onRequestChangeTeam"},
    ]

    _acp = null
    _seconds = 0
    _timer = null
    _next_map = null
    _previous_map = null

    constructor(context, seconds, previous_map = null) {
        base.constructor(context)
        this._acp = LobbyACP(this)
        this._seconds = seconds
        this._previous_map = previous_map
    }

    function fetchRandomMap(definitions) {
        local keys = get_table_keys(definitions)
        local instance = keys[random_int(0, keys.len() - 1)]

        // If more than one, then simply find different map!
        if (keys.len() > 1 && this._previous_map) {
            while (instance == this._previous_map.instance) {
                instance = keys[random_int(0, keys.len() - 1)]
            }
        }
        
        return Map.create(instance)
    }

    function updateMap(instance) {
        this._next_map = Map.create(instance)

        local message = LobbyMessage()
        message.setSeconds(this._seconds)
        message.setMap(this._next_map.instance)
        message.setRedPlayers(calculate_team_players(TeamTag.RED, players.online))
        message.setBluePlayers(calculate_team_players(TeamTag.BLUE, players.online))

        foreach (player in players.online) {
            message.toPacket().send(player.id, RELIABLE)
        }
    }

    function updateTime(seconds) {
        this._seconds = seconds

        local message = LobbyMessage()
        message.setSeconds(this._seconds)

        foreach (player in players.online) {
            message.toPacket().send(player.id, RELIABLE)
        }
    }

    /////////////////////////////////////////
    ///	Events
    /////////////////////////////////////////

    function onEnter() {
        // Select randomly next map
        this._next_map = this.fetchRandomMap(map_definitions)

        local message = LobbyMessage()
        message.setSeconds(this._seconds)
        message.setMap(this._next_map.instance)
        message.setRedPlayers(calculate_team_players(TeamTag.RED, players.online))
        message.setBluePlayers(calculate_team_players(TeamTag.BLUE, players.online))

        foreach (player in players.online) {
            message.setTeam(player.team ? player.team.tag : TeamTag.NONE)
            message.toPacket().send(player.id, RELIABLE)
        }

        // Clock timer
        this._timer = setTimer(this.onTimer.bindenv(this), 1000, 0)
    }

    function onExit() {
        killTimer(this._timer)
    }

    function onTimer() {
        --this._seconds

        if (this._seconds <= 0) {
            local red_players = calculate_team_players(TeamTag.RED, players.online)
            local blue_players = calculate_team_players(TeamTag.BLUE, players.online)
            
            local start_game = true
            if (players.online.len() < 2) {
                sendMessageToAll(255, 100, 0, "Not enought players, to start game.")
                start_game = false
            } else if (red_players == 0 && blue_players == players.online.len()) {
                sendMessageToAll(255, 0, 0, "Team red has no players.")
                start_game = false
            } else if (blue_players == 0 && red_players == players.online.len()) {
                sendMessageToAll(0, 150, 255, "Team blue has no players.")
                start_game = false
            }

            if (!start_game) {
                this._seconds = LOBBY_TIME_SEC

                local message = LobbyMessage()
                message.setSeconds(this._seconds)

                local packet = message.toPacket()
                foreach (player in players.online) {
                    packet.send(player.id, RELIABLE)
                }
            } else {
                this.context.machine.change(GameplayState(this.context, this._next_map))
            }
        }
    }

    function onJoin(pid) {
        sendMessageToPlayer(pid, 255, 210, 0, format("Welcome on %s!", getHostname()))

        // Send info about current stage
        local message = LobbyMessage()
        message.setSeconds(this._seconds)
        message.setMap(this._next_map.instance)
        message.setRedPlayers(calculate_team_players(TeamTag.RED, players.online))
        message.setBluePlayers(calculate_team_players(TeamTag.BLUE, players.online))
        message.toPacket().send(pid, RELIABLE)
    }

    function onHit(pid, kid, dmg, type) {
        cancelEvent()
    }

    function onRequestChangeTeam(pid) {
        local player = players.get(pid)
        if (player) {
            local team_tag = player.team ? player.team.tag : TeamTag.NONE
            if (++team_tag >= TeamTag.MAX) {
                team_tag = TeamTag.NONE
            }

            // Setup metainfo and color
            player.setTeam(this._next_map.getTeam(team_tag))

            // Send info about current stage
            local message = LobbyMessage()
            message.setRedPlayers(calculate_team_players(TeamTag.RED, players.online))
            message.setBluePlayers(calculate_team_players(TeamTag.BLUE, players.online))

            local packet = message.toPacket()
            foreach (player in players.online) {
                if (pid != player.id) {
                    packet.send(player.id, RELIABLE_ORDERED)
                }
            }

            message.setTeam(team_tag)
            message.toPacket().send(pid, RELIABLE_ORDERED)
        }
    }

    function onCommand(pid, cmd, params) {
        this._acp.handle(pid, cmd, params)
    }

    function onMessage(pid, message) {
        foreach (player in players.online) {
            sendPlayerMessageToPlayer(pid, player.id, 255, 255, 255, message)
        }
    }
}