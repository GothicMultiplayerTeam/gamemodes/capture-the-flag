class FinishedState extends State {
    events = [
        {name = "onPlayerJoin", handler = "onJoin"},
        {name = "onPlayerHit", handler = "onHit"},
        {name = "onPlayerMessage", handler = "onMessage"},
    ]

    _winner_team = null
    _previous_map = null

    constructor(context, team_tag, previous_map) {
        base.constructor(context)
        this._winner_team = team_tag
        this._previous_map = previous_map
    }

    /////////////////////////////////////////
    ///	Events
    /////////////////////////////////////////

    function onEnter() {
        local packet = FinishedMessage(this._winner_team).toPacket()
        foreach (player in players.online) {
            packet.send(player.id, RELIABLE)
        }

        // Update logic timer
        setTimer(this.onTimeout.bindenv(this), 10000, 1)
    }

    function onExit() {
        // Clear assigned teams!
        foreach (player in players.online) {
            unspawnPlayer(player.id)
            setPlayerInstance(player.id, "PC_HERO") // Just clear everything

            player.reset()
        }
    }

    function onJoin(pid) {
        local packet = FinishedMessage(this._winner_team).toPacket()
        packet.send(pid, RELIABLE)
    }

    function onTimeout() {
        this.context.machine.change(LobbyState(this.context, LOBBY_TIME_SEC, this._previous_map))
    }

    function onHit(pid, kid, dmg, type) {
        cancelEvent()
    }

    function onMessage(pid, message) {
        foreach (player in players.online) {
            sendPlayerMessageToPlayer(pid, player.id, 255, 255, 255, message)
        }
    }
}