# Introduction

**Capture the flag** is a gamemode, which implements the gameplay in the form of battle that involves moving a flag from enemy base to home base to score points. In this gamemode the varius extra features were introduced like extra bonuses, which can be captured or level system which increases damage output for player.

# Rules

- To win round your team has to gain 2 points.
- To score a point, your team need to move enemy flag to your base.
- You can score point even if your flag was stolen.
- To capture flag player need to stand near to flag for 5 seconds.
- By hitting player when he carries the flag, the flag will drop on the ground and can be recaptured again. Flag will drop even if player is hitted by ally.
- When player from the same team as flag will recapture it, flag is moved back to base.
- To capture bonus player need to stand near to bonus for 10 seconds.
- Every bonus last for 60 seconds.
- Friendly fire is disabled.
- Every kill gives 50 experience.
- Every assists gives 25 expierence.
- Each level of advantage over another player increases damage by an additional 10%.
- You can heal yourself while standing on spawn for 15 seconds (+25% HP)
- By killing enemy player gets two extra health/mana potions.
- There are three maps available: Onar farm, Bengar farm and Khorinis - Upper town.
- Every dead increases respawn time by 3 seconds. Maximum respawn time is 60 seconds.

# Installation

Include `scripts.xml` to your project, this can be done by adding this line to your `config.xml` file.

Example:

```xml
<import src="gamemodes/capture-the-flag/scripts.xml" />
```

# License

This project is licensed under the GNU GPL v2 License - see the [LICENSE](LICENSE) file for details.
