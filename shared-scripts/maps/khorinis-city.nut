map_definitions.khorinis_city <- MapDefinition({
    name = "Khorinis - City",
    world = "NEWWORLD\\NEWWORLD.ZEN",
    map = {
        texture = "MAP_NEWWORLD_CITY.TGA",
        level_coords = [-6900, 11800, 21600, -9400]
    },
    teams = {
        [TeamTag.RED] = {
            flag = {
                x = -380.859,
                y = 27.3438,
                z = -4342.81,
                angle = 77
            },
            spawn = {
                x = -5769.38,
                y = 236.563,
                z = -5890.23,
                angle = 76
            },
            inventory = [
                {instance = "ITHE_2", amount = 1, equip = true },
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_BOW_L_04", amount = 1, equip = true},
                {instance = "ITAR_MIL_M", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITRW_ARROW", amount = 1000},
            ]
        },
        [TeamTag.BLUE] = {
            flag = {
                x = 9767.89,
                y = 468.203,
                z = 4738.98,
                angle = 245
            },
            spawn = {
                x = 11842.9,
                y = 320.547,
                z = 6595.08,
                angle = 241
            },
            inventory = [
                {instance = "ITHE_3", amount = 1, equip = true },
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_BOW_L_04", amount = 1, equip = true},
                {instance = "ITAR_SLD_H", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITRW_ARROW", amount = 1000},
            ]
        }
    },
    bonuses = [
        {
            x = -301.406,
            y = -7.8281,
            z = 3001.48,
            factor = BONUS_DAMAGE_FACTOR,
            bonus = BonusType.DAMAGE
        },
        {
            x = 6501.25,
            y = 418.203,
            z = -3074.06,
            factor = BONUS_SPEED_FACTOR,
            bonus = BonusType.SPEED
        }
    ]
})