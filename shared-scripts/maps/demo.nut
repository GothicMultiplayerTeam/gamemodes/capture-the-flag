map_definitions.demo <- MapDefinition({
    name = "Demo",
    world = "NEWWORLD\\NEWWORLD.ZEN",
    teams = {
        [TeamTag.RED] = {
            flag = {
                x = 408.417389,
                y = -7.457317,
                z = 391.499268,
                angle = 187
            },
            spawn = {
                x = 438.074707,
                y = -67.407433,
                z = 940.404419,
                angle = 187
            },
            inventory = [
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_BOW_L_04", amount = 1, equip = true},
                {instance = "ITAR_MIL_M", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 5},
                {instance = "ITRW_ARROW", amount = 1000},
            ]
        },
        [TeamTag.BLUE] = {
            flag = {
                x = 24.133976,
                y = 17.142410,
                z = -1912.152832,
                angle = 14
            },
            spawn = {
                x = -142.863068,
                y = -84.587753,
                z = -2524.642334,
                angle = 14
            },
            inventory = [
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_BOW_L_04", amount = 1, equip = true},
                {instance = "ITAR_SLD_H", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 5},
                {instance = "ITRW_ARROW", amount = 1000},
            ]
        }
    },
    bonuses = [
        {
            x = 0,
            y = 0,
            z = 0,
            factor = BONUS_DAMAGE_FACTOR,
            bonus = BonusType.DAMAGE
        },
        {
            x = 0,
            y = 0,
            z = -200,
            factor = BONUS_SPEED_FACTOR,
            bonus = BonusType.SPEED
        }
    ]
})