map_definitions.khorinis_upper_town <- MapDefinition({
    name = "Khorinis - Upper town",
    world = "NEWWORLD\\NEWWORLD.ZEN",
    map = {
        texture = "MAP_NEWWORLD_CITY.TGA",
        level_coords = [-6900, 11800, 21600, -9400]
    },
    teams = {
        [TeamTag.RED] = {
            flag = {
                x = 15922.1,
                y = 1098.125,
                z = -4182.97,
                angle = 32
            },
            spawn = {
                x = 16647.4,
                y = 998.125,
                z = -3728.98,
                angle = 296
            },
            inventory = [
                {instance = "ITMW_ADDON_STAB01", amount = 1, equip = true},
                {instance = "ITAR_KDF_H", amount = 1, equip = true},
                {instance = "ITRU_INSTANTFIREBALL", amount = 1, equip = true, slot = 0},
                {instance = "ITRU_LIGHTNINGFLASH", amount = 1, equip = true, slot = 1},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITPO_MANA_03", amount = 2},
            ]
        },
        [TeamTag.BLUE] = {
            flag = {
                x = 11629,
                y = 1105.94,
                z = 2315.7,
                angle = 182
            },
            spawn = {
                x = 12661.8,
                y = 995.625,
                z = 1858.98,
                angle = 204
            },
            inventory = [
                {instance = "ITMW_ADDON_STAB01", amount = 1, equip = true},
                {instance = "ITRU_INSTANTFIREBALL", amount = 1, equip = true, slot = 0},
                {instance = "ITRU_LIGHTNINGFLASH", amount = 1, equip = true, slot = 1},
                {instance = "ITAR_KDW_H", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITPO_MANA_03", amount = 2},
            ]
        }
    },
    bonuses = [
        {
            x = 14137.5,
            y = 1248.12,
            z = -1300.39,
            factor = BONUS_DAMAGE_FACTOR,
            bonus = BonusType.DAMAGE
        },
        {
            x = 11654.8,
            y = 1048.047,
            z = -3558.83,
            factor = BONUS_SPEED_FACTOR,
            bonus = BonusType.SPEED
        }
    ]
})