map_definitions.bengar <- MapDefinition({
    name = "Khorinis - Bengar farm",
    world = "NEWWORLD\\NEWWORLD.ZEN",
    map = {
        texture = "MAP_NEWWORLD.TGA",
        level_coords = [-28000, 50500, 95500, -42500]
    },
    teams = {
        [TeamTag.RED] = {
            flag = {
                x = 49507,
                y = 3326.72,
                z = -19739,
                angle = 226
            },
            spawn = {
                x = 52241.9,
                y = 3102.42,
                z = -19031.5,
                angle = 252
            },
            inventory = [
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_CROSSBOW_H_01", amount = 1, equip = true},
                {instance = "ITAR_MIL_M", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITRW_BOLT", amount = 1000},
            ]
        },
        [TeamTag.BLUE] = {
            flag = {
                x = 39509.4,
                y = 3070.55,
                z = -23589.2,
                angle = 61
            },
            spawn = {
                x = 37406.8,
                y = 3108.98,
                z = -25875,
                angle = 46
            },
            inventory = [
                {instance = "ITMW_1H_SPECIAL_04", amount = 5, equip = true},
                {instance = "ITRW_CROSSBOW_H_01", amount = 1, equip = true},
                {instance = "ITAR_SLD_H", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITRW_BOLT", amount = 1000},
            ]
        }
    },
    bonuses = [
        {
            x = 46020.3,
            y = 2937.57,
            z = -25436.2,
            factor = BONUS_DAMAGE_FACTOR,
            bonus = BonusType.DAMAGE
        },
        {
            x = 43111.6,
            y = 2762.97,
            z = -16980.9,
            factor = BONUS_SPEED_FACTOR,
            bonus = BonusType.SPEED
        }
    ]
})