map_definitions.onar <- MapDefinition({
    name = "Khorinis - Onar farm",
    world = "NEWWORLD\\NEWWORLD.ZEN",
    map = {
        texture = "MAP_NEWWORLD.TGA",
        level_coords = [-28000, 50500, 95500, -42500]
    },
    teams = {
        [TeamTag.RED] = {
            flag = {
                x = 73722.7,
                y = 3520,
                z = -7828.98,
                angle = 185
            },
            spawn = {
                x = 74108.2,
                y = 3362.81,
                z = -6193.59,
                angle = 195
            },
            inventory = [
                {instance = "ITHE_4", amount = 1, equip = true },
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_BOW_L_04", amount = 1, equip = true},
                {instance = "ITAR_MIL_M", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITRW_ARROW", amount = 1000},
            ]
        },
        [TeamTag.BLUE] = {
            flag = {
                x = 73847.5,
                y = 3527.11,
                z = -14815,
                angle = 0
            },
            spawn = {
                x = 74093.9,
                y = 3637.03,
                z = -16432.9,
                angle = 0
            },
            inventory = [
                {instance = "ITHE_1", amount = 1, equip = true },
                {instance = "ITMW_1H_SPECIAL_04", amount = 1, equip = true},
                {instance = "ITRW_BOW_L_04", amount = 1, equip = true},
                {instance = "ITAR_SLD_H", amount = 1, equip = true},
                {instance = "ITPO_HEALTH_03", amount = 4},
                {instance = "ITRW_ARROW", amount = 1000},
            ]
        }
    },
    bonuses = [
        {
            x = 75865.7,
            y = 3652.27,
            z = -11299,
            factor = BONUS_DAMAGE_FACTOR,
            bonus = BonusType.DAMAGE
        },
        {
            x = 71224.6,
            y = 3278.98,
            z = -11378,
            factor = BONUS_SPEED_FACTOR,
            bonus = BonusType.SPEED
        }
    ]
})