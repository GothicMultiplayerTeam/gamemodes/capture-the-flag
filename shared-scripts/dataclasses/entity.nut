// Packet flags
const ENTITY_POSITION_FLAG = 1
const ENTITY_TEAM_FLAG = 2
const ENTITY_BONUS_FLAG = 4
const ENTITY_FOLLOW_FLAG = 8
const ENTITY_CAPTURE_FLAG = 16

//---------------------------------------

local function entity_tag(entity) {
    local tag = -1
    if (entity.hasComponent(FlagTag)) {
        tag = EntityTag.FLAG
    } else if (entity.hasComponent(BonusPointTag)) {
        tag = EntityTag.BONUS_POINT
    }

    return tag
}

class EntityData {
    idx = -1
    tag = -1
    position = null
    team_tag = null
    bonus = null
    follow_id = null
    capture_time_ms = null

    static function create(entity) {
        local data = this()
        data.idx = entity.idx
        data.tag = entity_tag(entity)

        local position = entity.getComponent(Position)
        if (position) data.position = position

        local team = entity.getComponent(Team)
        if (team) data.team_tag = team.tag

        local bonus = entity.getComponent(Bonus)
        if (bonus) data.bonus = bonus

        local follow = entity.getComponent(Follow)
        if (follow) data.follow_id = follow.npc_id

        local capture = entity.getComponent(Capture)
        if (capture) data.capture_time_ms = capture.time_ms

        return data
    }

    function write(packet, flags = 0) {
        packet.writeUInt8(this.idx)
        packet.writeUInt8(this.tag)
        
        // Recognize flags automatically
        if (flags == 0) {
            if (this.position != null) flags = flags | ENTITY_POSITION_FLAG
            if (this.team_tag != null) flags = flags | ENTITY_TEAM_FLAG
            if (this.bonus != null) flags = flags | ENTITY_BONUS_FLAG
            if (this.follow_id != null) flags = flags | ENTITY_FOLLOW_FLAG
            if (this.capture_time_ms != null) flags = flags | ENTITY_CAPTURE_FLAG
        } else {
            // Correct flags value if some components are missing
            if (flags & ENTITY_POSITION_FLAG && this.position == null) flags = flags ^ ENTITY_POSITION_FLAG
            if (flags & ENTITY_TEAM_FLAG && this.team_tag == null) flags = flags ^ ENTITY_TEAM_FLAG
            if (flags & ENTITY_FOLLOW_FLAG && this.follow_id == null) flags = flags ^ ENTITY_FOLLOW_FLAG
            if (flags & ENTITY_CAPTURE_FLAG && this.capture_time_ms == null) flags = flags ^ ENTITY_CAPTURE_FLAG
        }
        
        packet.writeUInt8(flags)

        if (flags & ENTITY_POSITION_FLAG) {
            packet.writeFloat(this.position.x)
            packet.writeFloat(this.position.y)
            packet.writeFloat(this.position.z)
        }

        if (flags & ENTITY_TEAM_FLAG) {
            packet.writeUInt8(this.team_tag)
        }

        if (flags & ENTITY_BONUS_FLAG) {
            packet.writeUInt8(this.bonus.type)
            packet.writeFloat(this.bonus.factor)
        }

        if (flags & ENTITY_FOLLOW_FLAG) {
            packet.writeInt8(this.follow_id)
        }

        if (flags & ENTITY_CAPTURE_FLAG) {
            packet.writeUInt16(this.capture_time_ms)
        }
    }

    static function read(packet) {
        local data = this()
        data.idx = packet.readUInt8()
        data.tag = packet.readUInt8()
 
        local flags = packet.readUInt8()
        if (flags & ENTITY_POSITION_FLAG) {
            data.position = Position(
                packet.readFloat(),
                packet.readFloat(),
                packet.readFloat()
            )
        }

        if (flags & ENTITY_TEAM_FLAG) {
            data.team_tag = packet.readUInt8()
        }

        if (flags & ENTITY_BONUS_FLAG) {
            data.bonus = {
                type = packet.readUInt8(),
                factor = packet.readFloat()
            }
        }

        if (flags & ENTITY_FOLLOW_FLAG) {
            data.follow_id = packet.readInt8()
        }

        if (flags & ENTITY_CAPTURE_FLAG) {
            data.capture_time_ms = packet.readUInt16()
        }

        return data
    }
}