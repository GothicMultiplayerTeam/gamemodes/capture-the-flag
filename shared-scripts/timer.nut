class Timestep {
    _last_ms = 0
    delta_ms = 0
    elapsed_ms = 0

    constructor() {
        this._last_ms = getTickCount()
    }

    function reset() {
        this.elapsed_ms = 0
    }

    function update() {
        local current_ms = getTickCount()

        this.delta_ms = (current_ms - this._last_ms)
        this.elapsed_ms += this.delta_ms
        this._last_ms = current_ms
    }
}
