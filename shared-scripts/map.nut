class MapDefinition {
    data = null

    constructor(data) {
        this.data = data
        this.validate()
    }

    function validate() {
        if (!("name" in this.data)) {
            throw "Missing 'name' key!"
        }
        
        if (!("world" in this.data)) {
            throw "Missing 'world' key!"
        }

        if (!("teams" in this.data)) {
            throw "Missing 'teams' key!"
        }

        if (!("bonuses" in this.data)) {
            throw "Missing 'bonuses' key!"
        }

        foreach (team in this.data.teams) {
            this._validateTeam(team)
        }

        foreach (item in data.bonuses) {
            this._validateBonus("bonusses", item)
        }
    }

    function _validateTeam(data) {
        if (!("flag" in data)) {
            throw "Missing 'teams.flag' key!"
        }

        if (!("spawn" in data)) {
            throw "Missing 'teams.spawn' key!"
        }

        if (!("inventory" in data)) {
            throw "Missing 'teams.inventory' key!"
        }

        this._validatePlacement("flag", data.flag)
        this._validatePlacement("spawn", data.spawn)

        foreach (item in data.inventory) {
            this._validateItem("inventory", item)
        }
    }

    function _validatePlacement(key, data) {
        if (!("x" in data)) {
            throw "Missing '" + key + ".x' key!"
        }

        if (!("y" in data)) {
            throw "Missing '" + key + ".x' key!"
        }

        if (!("z" in data)) {
            throw "Missing '" + key + ".x' key!"
        }

        if (!("angle" in data)) {
            throw "Missing '" + key + ".x' key!"
        }
    }

    function _validateBonus(key, item) {
        if (!("x" in item)) {
            throw "Missing '" + key + ".x' key!"
        }

        if (!("y" in item)) {
            throw "Missing '" + key + ".x' key!"
        }

        if (!("z" in item)) {
            throw "Missing '" + key + ".x' key!"
        }

        if (!("factor" in item)) {
            throw "Missing '" + key + ".factor' key!"
        }

        if (!("bonus" in item)) {
            throw "Missing '" + key + ".bonus' key!"
        }
    }

    function _validateItem(key, data) {
        if (!("instance" in data)) {
            throw "Missing '" + key + ".instance' key!"
        }

        if (!("amount" in data)) {
            throw "Missing '" + key + ".amount' key!"
        }
    }
}

// Register all definitions here!
map_definitions <- {}