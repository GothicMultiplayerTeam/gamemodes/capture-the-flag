enum PacketHeader {
    LOBBY,
    GAMEPLAY,
    FINISHED,
    SCORE,
    STATISTICS,
    DEAD,
    MESSAGE,
    ENTITY,
    COMPONENT,
    TEAM_CHANGE
}

enum ComponentHeader {
    CAPTURE,
    FOLLOW,
    REACH_DESTINATION,
    TEAM
}

enum EntityTag {
    FLAG,
    BONUS_POINT
}

enum TeamTag {
    NONE,
    RED,
    BLUE,
    // Info about size
    MAX
}

enum BonusType {
    DAMAGE,
    SPEED,
    REGENERATION,
    // Info about size
    MAX
}

/////////////////////////////////////////
///	Components
/////////////////////////////////////////

enum CaptureState {
    START,
    STOP,
    FINISH
}