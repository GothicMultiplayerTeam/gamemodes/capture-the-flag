class Scene {
    entities = null

    constructor() {
        this.entities = []
    }

    function create() {
        local entity = Entity()

        entity.idx = this.entities.len()
        this.entities.push(entity)

        return entity
    }

    function destroy(entity) {
        if (entity.idx == -1) {
            return
        }

        local lenght = this.entities.len()
        this.entities[entity.idx] = this.entities[length - 1]
        this.entities.pop()

        entity.idx = -1 // Invalidate
    }

    function clear() {
        this.entities.clear()
    }
}
