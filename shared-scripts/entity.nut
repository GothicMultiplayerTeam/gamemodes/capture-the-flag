local index = 0

class Entity {
    idx = -1
    _components = null

    constructor(idx = null) {
        this.idx = idx != null ? idx : index++
        this._components = {}
    }

    function hasComponent(component) {
        return component in this._components
    }

    function getComponent(component) {
        return component in this._components ? this._components[component] : null
    }

    function addComponent(instance) {
        if (instance.getclass() in this._components) {
            throw "Component already used!"
        }

        this._components[instance.getclass()] <- instance
    }

    function removeComponent(component) {
        delete this._components[component]
    }
}
