function random_float(min, max) {
    local roll = min + (1.0 * (max - min) * rand() / RAND_MAX)
    return roll
}

function random_int(min, max) {
    local roll = min + (1.0 * ((max + 1) - min) * rand() / RAND_MAX)
    return roll.tointeger()
}

function get_table_keys(table) {
    local keys = []
    foreach (key, _ in table) {
        keys.push(key)
    }

    return keys
}

function get_team_color(team_tag) {
    switch (team_tag) {
    case TeamTag.RED: return {r = 255, g = 0, b = 0}
    case TeamTag.BLUE: return {r = 0, g = 150, b = 255}
    }

    return {r = 255, g = 255, b = 255}
}

function get_team_name(team_tag) {
    switch (team_tag) {
    case TeamTag.RED: return "Red Team"
    case TeamTag.BLUE: return "Blue Team"
    }
}