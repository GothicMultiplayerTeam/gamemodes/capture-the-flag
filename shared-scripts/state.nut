class State {
    _closures = null

    context = null
    events = null

    constructor(context) {
        this.context = context
        this._closures = {}
    }

    function registerEvents() {
        if (this.events) {
            foreach (event in this.events) {
                local clousure = this[event.handler].bindenv(this)

                this._closures[event.handler] <- clousure
                addEventHandler(event.name, clousure)
            }
        }
    }

    function unregisterEvents() {
        if (this.events) {
            foreach (event in this.events) {
                removeEventHandler(event.name, this._closures[event.handler])
            }
        }
    }

    function onEnter() {
        // Called when we enter this state
    }

    function onExit() {
        // Callec before exit from this state
    }

    function onUpdate() {
        // Called every 1 sec
    }
}


class StateMachine {
    states = null

    constructor() {
        this.states = []
    }

    function change(state) {
        this.pop()
        this.push(state)
    }

    function push(state) {
        this.states.push(state)
        state.registerEvents()
        state.onEnter()
    }

    function pop() {
        if (this.states.len() > 0) {
            local state = this.states.pop()
            state.onExit()
            state.unregisterEvents()
        }
    }

    function current() {
        local lenght = this.states.len()
        return lenght > 0 ? this.states[lenght - 1] : null
    }
}
