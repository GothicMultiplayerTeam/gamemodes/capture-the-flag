class PacketHandler {
    static _handlers = {}

    constructor() {
        throw "Invalid operation!"
    }

    static function bind(message_id, callback) {
        local handlers = message_id in this._handlers ? this._handlers[message_id] : []
        if (handlers.len() == 0) {
            this._handlers[message_id] <- handlers
        }

        handlers.push(callback)
    }

    static function _readHeader(packet) {
        // Well simply get first byte
        return packet.readUInt8()
    }

    static function getHandlers(message_id) {
        if (message_id in this._handlers) {
            return this._handlers[message_id]
        }

        return null
    }

    static function _clientHandler(packet) {
        local message_id = this._readHeader(packet)
        local handlers = this.getHandlers(message_id)

        if (handlers) {
            foreach (callback in handlers) {
                callback(message_id, packet)
            }
        }
    }

    static function _serverHandler(pid, packet) {
        local message_id = this._readHeader(packet)
        local handlers = this.getHandlers(message_id)

        if (handlers) {
            foreach (callback in handlers) {
                callback(pid, message_id, packet)
            }
        }
    }
}

//---------------------------------------------------

local packet_handler = SERVER_SIDE ? PacketHandler._serverHandler : PacketHandler._clientHandler

addEventHandler("onPacket", packet_handler.bindenv(PacketHandler))
