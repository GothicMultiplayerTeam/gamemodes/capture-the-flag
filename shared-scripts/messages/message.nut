class InfoMessage {
    r = 0
    g = 0
    b = 0
    message = null

    constructor(r, g, b, message) {
        this.r = r
        this.g = g
        this.b = b
        this.message = message
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.MESSAGE)
        packet.writeUInt8(this.r)
        packet.writeUInt8(this.g)
        packet.writeUInt8(this.b)
        packet.writeString(this.message)

        return packet
    }

    static function fromPacket(packet) {
        try {
            local r = packet.readUInt8()
            local g = packet.readUInt8()
            local b = packet.readUInt8()
            local message = packet.readString()

            return this(r, g, b, message)
        } catch (e) {
            return null
        }
    }
}

if (CLIENT_SIDE) {
    addEvent("gameplay:message")

    local function gameplay_message(message_id, packet) {
        local message = InfoMessage.fromPacket(packet)
        if (message) {
            callEvent("gameplay:message", message)
        }
    }

    PacketHandler.bind(PacketHeader.MESSAGE, gameplay_message)
}
