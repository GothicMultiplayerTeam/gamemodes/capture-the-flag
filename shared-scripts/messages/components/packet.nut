if (CLIENT_SIDE) {
    addEvent("entity:capture")
    addEvent("entity:follow")
    addEvent("entity:destination")
    addEvent("entity:team")

    local function component_update(message_id, packet) {
        local component_header = packet.readUInt8()
        if (component_header == ComponentHeader.CAPTURE) {
            local message = CaptureStateMessage.fromPacket(packet)
            if (message) {
                callEvent("entity:capture", message)
            }
        } else if (component_header == ComponentHeader.FOLLOW) {
            local message = FollowStateMessage.fromPacket(packet)
            if (message) {
                callEvent("entity:follow", message)
            }
        } else if (component_header == ComponentHeader.REACH_DESTINATION) {
            local message = DestinationStateMessage.fromPacket(packet)
            if (message) {
                callEvent("entity:destination", message)
            }
        } else if (component_header == ComponentHeader.TEAM) {
            local message = TeamStateMessage.fromPacket(packet)
            if (message) {
                callEvent("entity:team", message)
            }
        }
    }

    PacketHandler.bind(PacketHeader.COMPONENT, component_update)
}
