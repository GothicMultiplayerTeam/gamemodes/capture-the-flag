class TeamStateMessage {
    idx = -1
    team_tag = -1

    constructor(idx, team_tag) {
        this.idx = idx
        this.team_tag = team_tag
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.COMPONENT)
        packet.writeUInt8(ComponentHeader.TEAM)
        packet.writeUInt8(this.idx)
        packet.writeUInt8(this.team_tag)

        return packet
    }

    static function fromPacket(packet) {
        try {
            local idx = packet.readUInt8()
            local team_tag = packet.readUInt8()

            return this(idx, team_tag)

        } catch (e) {
            return null
        }
    }
}