class FollowStateMessage {
    idx = -1
    npc_id = -1

    // Extra data
    position = null

    constructor(idx, pid, position = null) {
        this.idx = idx
        this.npc_id = pid
        this.position = position
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.COMPONENT)
        packet.writeUInt8(ComponentHeader.FOLLOW)
        packet.writeUInt8(this.idx)
        packet.writeInt8(this.npc_id)

        if (this.npc_id == -1) {
            packet.writeFloat(this.position.x)
            packet.writeFloat(this.position.y)
            packet.writeFloat(this.position.z)
        }

        return packet
    }

    static function fromPacket(packet) {
        try {
            local idx = packet.readUInt8()
            local npc_id = packet.readInt8()

            local message = this(idx, npc_id)
            if (npc_id == -1) {
                message.position = Position(
                    packet.readFloat(),
                    packet.readFloat(),
                    packet.readFloat()
                )
            }

            return message

        } catch (e) {
            return null
        }
    }
}