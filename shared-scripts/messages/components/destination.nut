class DestinationStateMessage {
    idx = -1
    team_tag = -1
    npc_id = -1

    constructor(idx, team_tag, pid) {
        this.idx = idx
        this.team_tag = team_tag
        this.npc_id = pid
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.COMPONENT)
        packet.writeUInt8(ComponentHeader.REACH_DESTINATION)
        packet.writeUInt8(this.idx)
        packet.writeUInt8(this.team_tag)
        packet.writeUInt8(this.npc_id)

        return packet
    }

    static function fromPacket(packet) {
        try {
            local idx = packet.readUInt8()
            local team_tag = packet.readUInt8()
            local npc_id = packet.readUInt8()

            return this(idx, team_tag, npc_id)

        } catch (e) {
            return null
        }
    }
}