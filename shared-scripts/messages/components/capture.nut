class CaptureStateMessage {
    idx = -1
    state = null
    team_tag = null
    npc_id = -1
    action = 0

    // Extra data
    time_ms = null

    constructor(idx, state, pid, team_tag) {
        this.idx = idx
        this.state = state
        this.npc_id = pid
        this.team_tag = team_tag
    }


    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.COMPONENT)
        packet.writeUInt8(ComponentHeader.CAPTURE)
        packet.writeUInt8(this.idx)
        packet.writeUInt8(this.state)
        packet.writeUInt8(this.team_tag)
        packet.writeUInt8(this.npc_id)

        if (this.state == CaptureState.START) {
            packet.writeUInt16(this.time_ms)
        }

        return packet
    }

    static function fromPacket(packet) {
        try {
            local idx = packet.readUInt8()
            local state = packet.readUInt8()
            local team_tag = packet.readUInt8()
            local npc_id = packet.readUInt8()

            local message = this(idx, state, npc_id, team_tag)
            if (state == CaptureState.START) {
                message.time_ms = packet.readUInt16()
            }

            return message

        } catch (e) {
            return null
        }
    }
}