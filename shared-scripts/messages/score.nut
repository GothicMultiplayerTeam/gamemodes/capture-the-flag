local RED_SCORE_FLAG = 1
local BLUE_SCORE_FLAG = 2

class ScoreMessage {
    flags = 0
    red_score = null
    blue_score = null

    constructor() {
        this.red_score = null
        this.blue_score = null
    }

    function setRedScore(value) {
        this.red_score = value
        this.flags = this.flags | RED_SCORE_FLAG
    }

    function setBlueScore(value) {
        this.blue_score = value
        this.flags = this.flags | BLUE_SCORE_FLAG
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.SCORE)
        packet.writeUInt8(this.flags)

        if (this.flags & RED_SCORE_FLAG) {
            packet.writeUInt8(this.red_score)
        }

        if (this.flags & BLUE_SCORE_FLAG) {
            packet.writeUInt8(this.blue_score)
        }

        return packet
    }

    static function fromPacket(packet) {
        local message = this()

        try {
            message.flags = packet.readUInt8()

            if (message.flags & RED_SCORE_FLAG) {
                message.red_score = packet.readUInt8()
            }

            if (message.flags & BLUE_SCORE_FLAG) {
                message.blue_score = packet.readUInt8()
            }

            return message
        } catch (e) {
            return null
        }
    }
}

if (CLIENT_SIDE) {
    addEvent("gameplay:score")

    local function gameplay_score(message_id, packet) {
        local message = ScoreMessage.fromPacket(packet)
        if (message) {
            callEvent("gameplay:score", message)
        }
    }

    PacketHandler.bind(PacketHeader.SCORE, gameplay_score)
}
