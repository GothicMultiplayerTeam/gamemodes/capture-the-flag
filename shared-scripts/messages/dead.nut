class DeadMessage {
    seconds = 0

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.DEAD)
        packet.writeUInt8(this.seconds)

        return packet
    }

    static function fromPacket(packet) {
        try {
            local message = this()
            message.seconds = packet.readUInt8()

            return message
        } catch (e) {
            return null
        }
    }
}

if (CLIENT_SIDE) {
    addEvent("player:dead")

    local function player_dead(message_id, packet) {
        local message = DeadMessage.fromPacket(packet)
        if (message) {
            callEvent("player:dead", message)
        }
    }

    PacketHandler.bind(PacketHeader.DEAD, player_dead)
}
