class RequestTeamChangeMessage {
    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.TEAM_CHANGE)

        return packet
    }

    static function fromPacket(packet) {
        return this()
    }
}

if (SERVER_SIDE) {
    addEvent("team:change")

    local function request_team_change(pid, message_id, packet) {
        local message = RequestTeamChangeMessage.fromPacket(packet)
        if (message) {
            callEvent("team:change", pid)
        }
    }

    PacketHandler.bind(PacketHeader.TEAM_CHANGE, request_team_change)
}