local KILLS_FLAG = 1
local DEATHS_FLAG = 2
local ASSISTS_FLAG = 4
local LEVEL_FLAG = 8
local EXPERIENCE_FLAG = 16
local NEXT_EXPERIENCE_FLAG = 32

class StatisticsMessage {
    flags = 0
    kills = null
    deaths = null
    assists = null
    level = null
    experience = null
    next_experience = null

    constructor() {
        this.kills = null
        this.deaths = null
        this.assists = null
        this.level = null
        this.experience = null
        this.next_experience = null
    }

    function setKills(value) {
        this.kills = value
        this.flags = this.flags | KILLS_FLAG
    }

    function setDeaths(value) {
        this.deaths = value
        this.flags = this.flags | DEATHS_FLAG
    }

    function setAssists(value) {
        this.assists = value
        this.flags = this.flags | ASSISTS_FLAG
    }

    function setLevel(value) {
        this.level = value
        this.flags = this.flags | LEVEL_FLAG
    }

    function setExperience(value) {
        this.experience = value
        this.flags = this.flags | EXPERIENCE_FLAG
    }

    function setNextExperience(value) {
        this.next_experience = value
        this.flags = this.flags | NEXT_EXPERIENCE_FLAG
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.STATISTICS)
        packet.writeUInt8(this.flags)

        if (this.flags & KILLS_FLAG) {
            packet.writeUInt8(this.kills)
        }

        if (this.flags & DEATHS_FLAG) {
            packet.writeUInt8(this.deaths)
        }

        if (this.flags & ASSISTS_FLAG) {
            packet.writeUInt8(this.assists)
        }

        if (this.flags & LEVEL_FLAG) {
            packet.writeUInt8(this.level)
        }

        if (this.flags & EXPERIENCE_FLAG) {
            packet.writeInt32(this.experience)
        }

        if (this.flags & NEXT_EXPERIENCE_FLAG) {
            packet.writeInt32(this.next_experience)
        }

        return packet
    }

    static function fromPacket(packet) {
        local message = this()

        try {
            message.flags = packet.readUInt8()

            if (message.flags & KILLS_FLAG) {
                message.kills = packet.readUInt8()
            }

            if (message.flags & DEATHS_FLAG) {
                message.deaths = packet.readUInt8()
            }

            if (message.flags & ASSISTS_FLAG) {
                message.assists = packet.readUInt8()
            }

            if (message.flags & LEVEL_FLAG) {
                message.level = packet.readUInt8()
            }

            if (message.flags & EXPERIENCE_FLAG) {
                message.experience = packet.readInt32()
            }

            if (message.flags & NEXT_EXPERIENCE_FLAG) {
                message.next_experience = packet.readInt32()
            }

            return message
        } catch (e) {
            return null
        }
    }
}

if (CLIENT_SIDE) {
    addEvent("player:statistics")

    local function player_statistics(message_id, packet) {
        local message = StatisticsMessage.fromPacket(packet)
        if (message) {
            callEvent("player:statistics", message)
        }
    }

    PacketHandler.bind(PacketHeader.STATISTICS, player_statistics)
}
