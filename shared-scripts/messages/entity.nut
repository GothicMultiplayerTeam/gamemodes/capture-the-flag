class EntityUpdateMessage {
    data = null

    constructor(data) {
        this.data = data
    }

    function toPacket(flags = 0) {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.ENTITY)
        this.data.write(packet, flags)

        return packet
    }

    static function fromPacket(packet) {
        try {
            local data = EntityData.read(packet)
            return this(data)

        } catch (e) {
            return null
        }
    }
}

if (CLIENT_SIDE) {
    addEvent("entity:update")

    local function entity_update(message_id, packet) {
        local message = EntityUpdateMessage.fromPacket(packet)
        if (message) {
            callEvent("entity:update", message.data)
        }
    }

    PacketHandler.bind(PacketHeader.ENTITY, entity_update)
}
