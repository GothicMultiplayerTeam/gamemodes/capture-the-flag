local TIMER_FLAG = 1
local TEAM_FLAG = 2
local RED_PLAYERS_FLAG = 4
local BLUE_PLAYERS_FLAG = 8
local MAP_FLAG = 16

class LobbyMessage {
    flags = 0
    seconds = null
    map = null
    team = null
    red_players = null
    blue_players = null

    function setSeconds(count) {
        this.seconds = count
        this.flags = this.flags | TIMER_FLAG
    }

    function setTeam(value) {
        this.team = value
        this.flags = this.flags | TEAM_FLAG
    }

    function setRedPlayers(value) {
        this.red_players = value
        this.flags = this.flags | RED_PLAYERS_FLAG
    }

    function setBluePlayers(value) {
        this.blue_players = value
        this.flags = this.flags | BLUE_PLAYERS_FLAG
    }

    function setMap(value) {
        this.map = value
        this.flags = this.flags | MAP_FLAG
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.LOBBY)
        packet.writeUInt8(this.flags)

        if (this.flags & TIMER_FLAG) {
            packet.writeUInt16(this.seconds)
        }

        if (this.flags & TEAM_FLAG) {
            packet.writeUInt8(this.team)
        }

        if (this.flags & RED_PLAYERS_FLAG) {
            packet.writeInt8(this.red_players)
        }

        if (this.flags & BLUE_PLAYERS_FLAG) {
            packet.writeInt8(this.blue_players)
        }

        if (this.flags & MAP_FLAG) {
            packet.writeString(this.map)
        }

        return packet
    }

    static function fromPacket(packet) {
        local message = this()

        try {
            local flags = packet.readUInt8()

            if (flags & TIMER_FLAG) {
                message.seconds = packet.readUInt16()
            }

            if (flags & TEAM_FLAG) {
                message.team = packet.readUInt8()
            }

            if (flags & RED_PLAYERS_FLAG) {
                message.red_players = packet.readUInt8()
            }

            if (flags & BLUE_PLAYERS_FLAG) {
                message.blue_players = packet.readUInt8()
            }

            if (flags & MAP_FLAG) {
                message.map = packet.readString()
            }

        } catch (e) {
            return null
        }

        return message
    }
}

if (CLIENT_SIDE) {
    addEvent("state:lobby")

    local function lobby_state(message_id, packet) {
        local message = LobbyMessage.fromPacket(packet)
        if (message) {
            callEvent("state:lobby", message)
        }
    }

    PacketHandler.bind(PacketHeader.LOBBY, lobby_state)
}
