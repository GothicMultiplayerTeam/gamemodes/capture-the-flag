class FinishedMessage {
    winner_team = null

    constructor(team_tag) {
        this.winner_team = team_tag
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.FINISHED)
        packet.writeUInt8(this.winner_team)

        return packet
    }

    static function fromPacket(packet) {
        try {
            local winning_team = packet.readUInt8()
            return this(winning_team)
        } catch (e) {
            return null
        }
    }
}

if (CLIENT_SIDE) {
    addEvent("state:finished")

    local function finished_state(message_id, packet) {
        local message = FinishedMessage.fromPacket(packet)
        if (message) {
            callEvent("state:finished", message)
        }
    }

    PacketHandler.bind(PacketHeader.FINISHED, finished_state)
}
