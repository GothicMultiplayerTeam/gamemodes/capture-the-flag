local RED_SCORE_FLAG = 1
local BLUE_SCORE_FLAG = 2
local MAP_FLAG = 4
local ENTITIES_FLAG = 8

class GameplayInitMessage {
    flags = 0
    map = null
    red_score = null
    blue_score = null
    entities = null

    function setRedScore(value) {
        this.red_score = value
        this.flags = this.flags | RED_SCORE_FLAG
    }

    function setBlueScore(value) {
        this.blue_score = value
        this.flags = this.flags | BLUE_SCORE_FLAG
    }

    function setMap(value) {
        this.map = value
        this.flags = this.flags | MAP_FLAG
    }

    function insertEntities(entities) {
        this.entities = []
        this.flags = this.flags | ENTITIES_FLAG

        foreach (entity in entities) {
            this.entities.push(EntityData.create(entity))
        }
    }

    function toPacket() {
        local packet = Packet()
        packet.writeUInt8(PacketHeader.GAMEPLAY)
        packet.writeUInt8(this.flags)

        if (this.flags & RED_SCORE_FLAG) {
            packet.writeInt8(this.red_score)
        }

        if (this.flags & BLUE_SCORE_FLAG) {
            packet.writeInt8(this.blue_score)
        }

        if (this.flags & MAP_FLAG) {
            packet.writeString(this.map)
        }

        if (this.flags & ENTITIES_FLAG) {
            packet.writeUInt8(this.entities.len())
            foreach (entity in this.entities) {
                entity.write(packet)
            }
        }

        return packet
    }

    static function fromPacket(packet) {
        local message = this()

        try {
            message.flags = packet.readUInt8()

            if (message.flags & RED_SCORE_FLAG) {
                message.red_score = packet.readUInt8()
            }

            if (message.flags & BLUE_SCORE_FLAG) {
                message.blue_score = packet.readUInt8()
            }

            if (message.flags & MAP_FLAG) {
                message.map = packet.readString()
            }

            if (message.flags & ENTITIES_FLAG) {
                message.entities = []

                local len = packet.readUInt8()
                for (local i = 0; i < len; ++i) {
                    message.entities.push(EntityData.read(packet))
                }
            }
        } catch (e) {
            return null
        }

        return message
    }
}

if (CLIENT_SIDE) {
    addEvent("state:gameplay")

    local function gameplay_state(message_id, packet) {
        local message = GameplayInitMessage.fromPacket(packet)
        if (message) {
            callEvent("state:gameplay", message)
        }
    }

    PacketHandler.bind(PacketHeader.GAMEPLAY, gameplay_state)
}
